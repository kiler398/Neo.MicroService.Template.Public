﻿using Microsoft.EntityFrameworkCore;
using Shouldly;
using System;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Xunit;
using NEO.MicroService.Domain;
using NEO.MicroService.Repositories;

namespace NEO.MicroService.EntityFrameworkCore.Repositories
{
    /* This is just an example test class.
     * Normally, you don't test ABP framework code
     * (like default AppUser repository IRepository<AppUser, Guid> here).
     * Only test your custom repository methods.
     */
    public class MicroServiceUserRepositoryTests : MicroServiceEntityFrameworkCoreTestBase
    {
        private readonly IMicroServiceUserRepository _repository;

        public MicroServiceUserRepositoryTests()
        {
            _repository = GetRequiredService<IMicroServiceUserRepository>();
        }

        [Fact]
        public async Task Should_Query_AppUser()
        {
            /* Need to manually start Unit Of Work because
             * FirstOrDefaultAsync should be executed while db connection / context is available.
             */
            await WithUnitOfWorkAsync(async () =>
            {
                //Act
                var datas = await (await _repository.GetQueryableAsync())
                    //.Where(u => u.UserName == "admin")
                    .ToListAsync();

                //Assert
                Assert.True(datas.Count > 0);
            });
        }
    }
}
