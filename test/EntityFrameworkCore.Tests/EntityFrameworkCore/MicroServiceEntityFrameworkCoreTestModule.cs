﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Neo.Abp.EntityFrameworkCore;
using Neo.Abp.EntityFrameworkCore.MySQL;
using System.IO;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace NEO.MicroService.EntityFrameworkCore;

[DependsOn(
        typeof(MicroServiceEntityFrameworkCoreModule),
        typeof(MicroServiceTestBaseModule),
        typeof(NeoAbpEntityFrameworkCoreMySQLModule)
    )]
public class MicroServiceEntityFrameworkCoreTestModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        ConfigureDatabase(context.Services);
    }

    private static void ConfigureDatabase(IServiceCollection services)
    {
        var configuration = BuildConfiguration();
 
        var abpDbContextConfigurator = services.GetRequiredService<IAbpDbContextConfigurator>();

        services.Configure<AbpDbContextOptions>(options =>
        {
            options.Configure(context =>
            {
                abpDbContextConfigurator.UseDatabase(context.DbContextOptions, configuration.GetConnectionString("Default"));
            });
        });
    }

    public override void OnApplicationShutdown(ApplicationShutdownContext context)
    {

    }

    private static IConfigurationRoot BuildConfiguration()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Path.Combine(Directory.GetCurrentDirectory()))
            .AddJsonFile("appsettings.json", optional: false);

        return builder.Build();
    }
}
