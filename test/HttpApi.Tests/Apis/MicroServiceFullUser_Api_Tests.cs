using System;
using System.Threading.Tasks;
using Bogus;
using Shouldly;
using NEO.MicroService.Dtos;
using Xunit;
using NEO.MicroService.Controllers;

namespace NEO.MicroService.Pages
{
    /// <summary>
    /// API集成测试类
    /// </summary>
    /// <example>
    /// 利用Bogus模拟测试数据调用Create方法
    /// <code>
    /// [Fact]
    /// public async Task TestCreateByBogus()
    /// {
    ///     var tenantId = System.Guid.NewGuid();
    ///     var testData = new Faker<MicroServiceFullUserCreateUpdateInput>()
    ///     .StrictMode(true)
    ///     .RuleFor(o => o.TenantId, f => tenantId)
    ///     .RuleFor(o => o.Email, f => f.Internet.Email())
    ///     .RuleFor(o => o.UserName, f => f.Internet.UserName())
    ///     .RuleFor(o => o.Name, f => f.Name.FullName())
    ///     .RuleFor(o => o.Surname, f => f.Name.FullName())
    ///     .RuleFor(o => o.EmailConfirmed, f => true)
    ///     .RuleFor(o => o.PhoneNumber, f => f.Phone.PhoneNumber())
    ///     .RuleFor(o => o.PhoneNumberConfirmed, true).Generate();
    ///     
    ///    var result = await _microServiceUserController.Create(testData); 
    /// }
    /// </code>
    /// 手工赋值数据调用Create方法
    /// <code>
    /// [Fact]
    /// public async Task TestCreate()
    /// {
    ///    var tenantId = System.Guid.NewGuid();
    ///
    ///    var testData = new MicroServiceFullUserCreateUpdateInput()
    ///    {
    ///        TenantId = tenantId,
    ///        Email = "test111@gus.com",
    ///        UserName = "test11122",
    ///        Name = "test111",
    ///        Surname = "test11122",
    ///        EmailConfirmed = true,
    ///        PhoneNumber = "1212121212121",
    ///        PhoneNumberConfirmed = true,
    ///    };
    ///    
    ///    var result = await _microServiceUserController.Create(testData);
    /// }
    /// </code>
    /// </example>
    public class MicroServiceFullUser_Api_Tests : MicroServiceHttpApiTestBase
    {
        private readonly MicroServiceFullUserController _microServiceUserController;

        public MicroServiceFullUser_Api_Tests()
        {
            _microServiceUserController = GetRequiredService<MicroServiceFullUserController>();
        }

        [Fact]
        public async Task TestQueryCount()
        {
            var result = await _microServiceUserController.QueryCount(new Dtos.MicroServiceFullUserGetListInput());

            Assert.NotNull(result);
        }
    }
}
