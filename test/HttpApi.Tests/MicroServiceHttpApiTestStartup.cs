﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Volo.Abp.Uow;

namespace NEO.MicroService
{
    public class MicroServiceHttpApiTestStartup
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddApplication<MicroServiceHttpApiTestModule>();
            services.Configure<AbpUnitOfWorkDefaultOptions>(options =>
            {
                options.TransactionBehavior = UnitOfWorkTransactionBehavior.Auto;
            });
        }

        public static void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            app.InitializeApplication();
        }
    }
}