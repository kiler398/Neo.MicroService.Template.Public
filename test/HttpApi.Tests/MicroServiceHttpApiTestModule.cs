using Localization.Resources.AbpUi;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Globalization;
using NEO.MicroService.Localization;
using Volo.Abp.AspNetCore.TestBase;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Validation.Localization;
using Neo.Controllers;
using Neo.HttpApi.Host.Ioc;

namespace NEO.MicroService
{
    [DependsOn(
        typeof(AbpAspNetCoreTestBaseModule),
        typeof(MicroServiceHttpApiModule),
        typeof(MicroServiceApplicationTestModule)
    )]
    public class MicroServiceHttpApiTestModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            // �Զ�ע��
            context.Services.AutoInject(typeof(BaseController), ServiceLifetime.Singleton);
            ConfigureLocalizationServices(context.Services);
        }

        private static void ConfigureLocalizationServices(IServiceCollection services)
        {
            var cultures = new List<CultureInfo> { new CultureInfo("en"), new CultureInfo("tr") };
            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture("en");
                options.SupportedCultures = cultures;
                options.SupportedUICultures = cultures;
            });

            services.Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Get<MicroServiceResource>()
                    .AddBaseTypes(
                        typeof(AbpValidationResource),
                        typeof(AbpUiResource)
                    );
            });
        }
    }
}
