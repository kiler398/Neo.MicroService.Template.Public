﻿using Volo.Abp.Modularity;

namespace NEO.MicroService;

[DependsOn(
    typeof(MicroServiceApplicationModule),
    typeof(MicroServiceDomainTestModule)
    )]
public class MicroServiceApplicationTestModule : AbpModule
{

}
