using NEO.MicroService.Services;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace NEO.MicroService.Applications
{
    /* This is just an example test class.
     * Normally, you don't test code of the modules you are using
     * (like IMicroServiceFullUserAppService here).
     * Only test your own application services.
     */
    public class MicroServiceFullUserAppServiceTests : MicroServiceApplicationTestBase
    {
        private readonly IMicroServiceFullUserAppService _appService;

        public MicroServiceFullUserAppServiceTests()
        {
            _appService = GetRequiredService<IMicroServiceFullUserAppService>();
        }

        [Fact]
        public async Task Initial_Data_Should_Contain_Admin_User()
        {
            ////Act
            var result = await _appService.GetListAsync(new Dtos.MicroServiceFullUserGetListInput());

            ////Assert
            Assert.True(result.TotalCount > 0);
        }
    }
}
