﻿using NEO.MicroService.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace NEO.MicroService;

[DependsOn(
    typeof(MicroServiceEntityFrameworkCoreTestModule)
    )]
public class MicroServiceDomainTestModule : AbpModule
{

}
