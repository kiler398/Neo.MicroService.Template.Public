using System.Threading.Tasks;
using Shouldly;
using NEO.MicroService.DomainServices;
using Xunit;

namespace NEO.MicroService.Domains
{
    /* This is just an example test class.
     * Normally, you don't test code of the modules you are using
     * (like IdentityUserManager here).
     * Only test your own domain services.
     */
    /// <summary>
    /// 领域逻辑测试类
    /// </summary>
    /// <example>
    /// 领域服务测试方法例子：
    /// <code>
    /// [Fact]
    /// public async Task Should_Set_Email_Of_A_User()
    /// {
    ///     IdentityUser adminUser;
    ///     
    ///     /* Need to manually start Unit Of Work because
    ///      * FirstOrDefaultAsync should be executed while db connection / context is available.
    ///      */
    ///      
    ///     await WithUnitOfWorkAsync(async () =>
    ///     {
    ///         adminUser = await _microServiceUserManager
    ///             .FindByNormalizedUserNameAsync("ADMIN");
    ///             
    ///         await _microServiceUserManager.SetEmailAsync(adminUser, "newemail@abp.io");
    ///      });
    ///      
    ///      adminUser = await _identityUserRepository.FindByNormalizedUserNameAsync("ADMIN");
    ///      adminUser.Email.ShouldBe("newemail@abp.io");  
    /// 
    /// }
    /// </code>
    /// </example>
    public class MicroServiceFullUserDomainTests : MicroServiceDomainTestBase
    {
        private readonly IMicroServiceFullUserManager _microServiceUserManager;

        public MicroServiceFullUserDomainTests()
        {
            _microServiceUserManager = GetRequiredService<IMicroServiceFullUserManager>();
        }


    }
}
