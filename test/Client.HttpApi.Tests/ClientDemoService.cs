﻿using System;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace NEO.MicroService.HttpApi.Client.ConsoleTestApp;

public class ClientDemoService : ITransientDependency
{


    public ClientDemoService()
    {

    }

    public Task RunAsync()
    {
        return Task.CompletedTask;
    }
}
