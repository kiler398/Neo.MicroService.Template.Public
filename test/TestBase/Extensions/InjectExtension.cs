﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;

namespace NEO.MicroService
{
    /// <summary>
    /// 依赖注入拓展
    /// </summary>
    public static class InjectExtension
    {
        /// <summary>
        /// 自动依赖注册（自动扫描程序集）
        /// </summary>
        /// <param name="services">服务容器</param>
        /// <param name="inheritType">父类（继承该类就会被注入）</param>
        /// <param name="serviceLifetime">注入生命周期</param>
        public static void AutoInject(this IServiceCollection services, [NotNull] Type inheritType, [NotNull] ServiceLifetime serviceLifetime)
        {
            var files = Directory.GetFiles(AppContext.BaseDirectory, "*.dll");
            var appAssemblies = files.Select(x => Assembly.LoadFrom(x)).Where(x =>
                !x.GetName().Name.StartsWith("Microsoft.", StringComparison.CurrentCultureIgnoreCase)
                && !x.GetName().Name.StartsWith("System.", StringComparison.CurrentCultureIgnoreCase)
                && !x.GetName().Name.StartsWith("runtime.", StringComparison.CurrentCultureIgnoreCase));
            foreach (var assembly in appAssemblies)
            {
                AutoInject(services, assembly, inheritType, serviceLifetime);
            }
        }

        /// <summary>
        /// 自动依赖注册
        /// </summary>
        /// <param name="services">服务容器</param>
        /// <param name="applicationAssembly">程序集</param>
        /// <param name="inheritType">父类（继承该类就会被注入）</param>
        /// <param name="serviceLifetime">注入生命周期</param>
        public static void AutoInject(this IServiceCollection services, [NotNull] Assembly applicationAssembly, [NotNull] Type inheritType, [NotNull] ServiceLifetime serviceLifetime)
        {
            var typeList = applicationAssembly.GetTypes()
                .Where(t => inheritType.IsAssignableFrom(t)
                && t.IsClass
                && t.IsPublic
                && !t.IsAbstract
                && !t.IsSealed);
            foreach (var serviceType in typeList)
            {
                InjectSingleServiceLifetime(services, serviceType, serviceLifetime);
            }
        }

        /// <summary>
        /// 注入单个服务生命周期
        /// </summary>
        /// <param name="services">服务容器</param>
        /// <param name="serviceType">注入类型</param>
        /// <param name="serviceLifetime"></param>
        /// <exception cref="Exception"></exception>
        private static void InjectSingleServiceLifetime([NotNull] IServiceCollection services, [NotNull] Type serviceType, [NotNull] ServiceLifetime serviceLifetime)
        {
            switch (serviceLifetime)
            {
                case ServiceLifetime.Singleton:
                    services.TryAddSingleton(serviceType);
                    break;
                case ServiceLifetime.Scoped:
                    services.TryAddScoped(serviceType);
                    break;
                case ServiceLifetime.Transient:
                    services.TryAddTransient(serviceType);
                    break;
                default:
                    throw new Exception($"【注入单个服务生命周期】找不到对应的生命周期serviceLifetime:{serviceLifetime}");
            }
        }
    }
}
