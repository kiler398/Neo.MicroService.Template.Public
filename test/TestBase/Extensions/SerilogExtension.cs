﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Neo.HttpApi.Host.Options;
using Serilog;
using System;
using System.IO;

namespace NEO.MicroService
{
    /// <summary>
    /// Serilog拓展方法
    /// </summary>
    public static class SerilogExtension
    {
        /// <summary>
        /// 程序配置
        /// </summary>
        private static IConfiguration _startUpConfiguration;

        /// <summary>
        /// 设置Serilog配置
        /// </summary>
        public static void SettingSerilog(this IServiceCollection services, string applicationName)
        {
            // 加载默认配置设置
            LoadDefaultSetting();

            //加载服务发现配置
            var serviceDiscoveryOption = _startUpConfiguration.GetOptionByName<ServiceDiscoveryOption>(ServiceDiscoveryOption.SectionName);
            var appDeployName = _startUpConfiguration["App:DeployName"];
            var systemVerison = File.ReadAllText(Path.Combine(AppContext.BaseDirectory, "Version.txt"));

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(_startUpConfiguration)
                .Enrich.WithProperty("ServiceName", serviceDiscoveryOption.ServiceName)
                .Enrich.WithProperty("DeployName", appDeployName)
                .Enrich.WithProperty("Verison", systemVerison)
                .Enrich.WithProperty("ServiceUrl", serviceDiscoveryOption.ServiceUrl)
                .Enrich.WithProperty("ApplicationName", applicationName)
                .Enrich.WithProperty("Environment", "Environment")
                .CreateLogger();

            // 替换默认注册日志实现类
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.ClearProviders();
                loggingBuilder.AddSerilog(dispose: true);
                loggingBuilder.SetMinimumLevel(LogLevel.Trace);
            });
        }

        /// <summary>
        /// 加载默认配置设置
        /// </summary>
        private static void LoadDefaultSetting()
        {
            _startUpConfiguration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
