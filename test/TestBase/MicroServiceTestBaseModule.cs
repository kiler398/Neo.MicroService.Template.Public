﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.Authorization;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Data;
using Volo.Abp.Modularity;
using Volo.Abp.Threading;
using Neo.HttpApi.Host.Host;
using Neo.Jwt;
using Neo.Identity.Core;
using System;
using Neo.Security;
using Volo.Abp.Caching;
using Volo.Abp.EventBus.RabbitMq;
using Volo.Abp.Caching.StackExchangeRedis;
using Neo.Abp.TestBase;

namespace NEO.MicroService;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(AbpCachingModule),
    typeof(AbpEventBusRabbitMqModule),
    typeof(AbpCachingStackExchangeRedisModule),
    typeof(AbpTestBaseModule),
    typeof(AbpAuthorizationModule),
    typeof(MicroServiceDomainModule)
    )]
public class MicroServiceTestBaseModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAlwaysAllowAuthorization();
        //配置JWT服务
        context.ConfigureJwtTokenService();
        //配置TONO身份模块,默认使用Guid方案
        context.SetupIdentity<Guid, Guid, AbpClaimsIdentityGuidUserGuidTenantValueExtract>(context.ReadIsConfigIdentity());
        //配置分布事件
        context.ConfigureEventBus();
        //配置缓存
        context.ConfigureCache("MicroService:");
    }

    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        SeedTestData(context);
    }

    private static void SeedTestData(ApplicationInitializationContext context)
    {
        AsyncHelper.RunSync(async () =>
        {
            using (var scope = context.ServiceProvider.CreateScope())
            {
                await scope.ServiceProvider
                    .GetRequiredService<IDataSeeder>()
                    .SeedAsync();
            }
        });
    }


}
