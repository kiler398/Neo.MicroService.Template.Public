﻿using Neo.Client.HttpApi;
using NEO.MicroService.Client.Contracts;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Text;

namespace NEO.MicroService
{
    public class MicroServiceHttpApiClientOptions : NeoHttpApiClientOptions
    {
        public override Type HttpApiClientContractsModuleType { get; set; } = typeof(MicroServiceHttpApiClientContractsModule);
        public override string RemoteServiceName { get; set; } = "MicroService";


        public MicroServiceHttpApiClientOptions()
        {
        }
    }



}
