using Microsoft.Extensions.DependencyInjection;
using Neo.Client.HttpApi;
using Polly;
using Refit;
using System;
using System.Collections.Generic;
using System.Net.Http;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;


namespace NEO.MicroService
{
    [DependsOn(
        typeof(AbpHttpClientModule)
    )]
    public class MicroServiceHttpApiClientModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var httpApiClientOptions = context.Services.ExecutePreConfiguredActions<MicroServiceHttpApiClientOptions>();

            context.Services.AddAbpHttpClientRefitServiceProxy(httpApiClientOptions);
        }
    }
}
