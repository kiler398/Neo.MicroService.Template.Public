using Neo.Client.Contracts;
using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NEO.MicroService.Client.Contracts
{
    /// <summary>
    /// 服务代理类
    /// <example>
    /// 生成服务代理类，代码样例
    /// <code>
    /// [Post("/rpc/MicroService/MicroServiceFullUserRpc")]
    /// Task<MicroServiceFullUserGetRpcOutputDto> Create(
    ///     [Body] MicroServiceFullUserCreateUpdateRpcInput createInput
    /// );
    /// 
    /// [Post("/rpc/MicroService/MicroServiceFullUserRpc/{id}/Update")]
    /// Task<ApiResultModel<MicroServiceFullUserGetRpcOutputDto>> Update(
    ///     Guid id,
    ///     [Body] MicroServiceFullUserCreateUpdateRpcInput updateInput
    /// );
    /// 
    /// [Post("/rpc/MicroService/MicroServiceFullUserRpc/{id}/Delete")]
    /// Task<ApiResultModel<dynamic>> Delete(
    ///         Guid id
    /// );
    /// 
    /// [Get("/rpc/MicroService/MicroServiceFullUserRpc/{id}")]
    /// Task<ApiResultModel<MicroServiceFullUserGetRpcOutputDto>> Get(Guid id);
    /// 
    /// [Post("/rpc/MicroService/MicroServiceFullUserRpc/Query")]
    /// Task<ApiResultModel<List<MicroServiceFullUserGetListRpcOutputDto>>> Query([Body] MicroServiceFullUserGetListRpcInput getListInput);
    /// 
    /// [Post("/rpc/MicroService/MicroServiceFullUserRpc/QueryCount")]
    /// Task<ApiResultModel<long>> QueryCount([Body] MicroServiceFullUserGetListInput getListInput);
    /// 
    /// [Post("/rpc/MicroService/MicroServiceFullUserRpc/PageQuery")]
    /// Task<ApiResultModel<MicroServiceFullUserRpcPagedResultDto>> PageQuery([Body] MicroServiceFullUserGetListPageRpcInput getListPageInput);
    /// </code>
    /// </example>
    /// </summary>
    public interface IMicroServiceFullUserServiceProxy : IRefitServiceProxy
    {
        [Post("/rpc/MicroService/MicroServiceFullUserRpc/TestRemoteRpc")]
        Task<Guid> TestRemoteRpc();
    }
}
