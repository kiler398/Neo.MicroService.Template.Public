using Neo.Client.Contracts;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NEO.MicroService.Client.Contracts
{
    [SwaggerSchema(Required = new[] { "明细数据DTO" })]
    public class MicroServiceFullUserGetRpcOutputDto : EntityDto<Guid>
    {

        [SwaggerSchema("用户名")]
        public string UserName { get; set; }
        [SwaggerSchema("名称")]
        public string Name { get; set; }
        [SwaggerSchema("简称")]
        public string Surname { get; set; }
        [SwaggerSchema("邮件")]
        public string Email { get; set; }
        [SwaggerSchema("邮件确认")]
        public bool EmailConfirmed { get; set; }
        [SwaggerSchema("手机号")]
        public string PhoneNumber { get; set; }
        [SwaggerSchema("手机确认")]
        public bool PhoneNumberConfirmed { get; set; }
    }
}
