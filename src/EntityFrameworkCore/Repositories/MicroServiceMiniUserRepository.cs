using NEO.MicroService.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Specifications;
using Neo.EntityFrameworkCore;
using NEO.MicroService.EntityFrameworkCore;
using NEO.MicroService.Domain;

namespace NEO.MicroService.Repositories
{
    public class MicroServiceMiniUserRepository : BaseEFCoreAbpRepository<IMicroServiceDbContext, MicroServiceMiniUser, Guid>, IMicroServiceMiniUserRepository
    {
        public MicroServiceMiniUserRepository(IDbContextProvider<IMicroServiceDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
