using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Specifications;
using NEO.MicroService.Domain;
using Neo.EntityFrameworkCore;
using NEO.MicroService.Repositories;
using NEO.MicroService.EntityFrameworkCore;

namespace NEO.MicroService.Repositories
{
    public class MicroServiceFullUserRepository : BaseEFCoreAbpRepository<IMicroServiceDbContext, MicroServiceFullUser, Guid>, IMicroServiceFullUserRepository
    {
        public MicroServiceFullUserRepository(IDbContextProvider<IMicroServiceDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
