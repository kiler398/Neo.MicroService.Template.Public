﻿using Microsoft.EntityFrameworkCore;
using NEO.MicroService.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace NEO.MicroService.EntityFrameworkCore
{
    [ConnectionStringName("Default")]
    public interface IMicroServiceDbContext : IEfCoreDbContext
    {
        DbSet<MicroServiceUser> Users { get; set; }

        DbSet<MicroServiceFullUser> FullUsers { get; set; }

        DbSet<MicroServiceMiniUser> MiniUsers { get; set; }

    }
}
