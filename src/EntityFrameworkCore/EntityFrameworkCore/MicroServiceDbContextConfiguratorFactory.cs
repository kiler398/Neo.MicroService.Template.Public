﻿using Neo.Abp.EntityFrameworkCore;
using Neo.Abp.EntityFrameworkCore.MySQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEO.MicroService.EntityFrameworkCore
{
    public class MicroServiceDbContextConfiguratorFactory : IAbpDbContextConfiguratorFactory
    {
        public IAbpDbContextConfigurator BuildDbContextConfigurator()
        {
            return new MySQLAbpDbContextConfigurator();
        }
    }
}
