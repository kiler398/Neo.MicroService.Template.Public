﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;
using System;
using NEO.MicroService.Domain;

namespace NEO.MicroService.EntityFrameworkCore
{
    public static class MicroServiceDbContextModelCreatingExtensions
    {
        /// <summary>
        /// 配置EFCore映射，一些实体标注没有办法覆盖到的映射配置在这里处理，比如字段默认值
        /// </summary>
        /// <example>
        /// Configure your own tables/entities inside here
        /// <code>
        /// builder.Entity<MicroServiceUser>(b =>{
        /// {
        ///     b.ConfigureByConvention();
        ///     b.Property(x => x.Email).HasDefaultValue("test@sina.com");
        /// });
        /// </code>
        /// </example>
        /// <param name="builder"></param>
        /// <param name="optionsAction"></param>
        public static void ConfigureMicroService(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */

            builder.Entity<MicroServiceUser>(b =>
            {
                b.ConfigureByConvention();
            });

            builder.Entity<MicroServiceFullUser>(b =>
            {
                b.ConfigureByConvention();
            });

            builder.Entity<MicroServiceMiniUser>(b =>
            {
                b.ConfigureByConvention();
            });
        }
    }
}