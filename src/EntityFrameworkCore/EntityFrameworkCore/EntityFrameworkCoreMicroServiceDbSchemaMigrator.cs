﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NEO.MicroService.Data;
using Volo.Abp.DependencyInjection;

namespace NEO.MicroService.EntityFrameworkCore;

public class EntityFrameworkCoreMicroServiceDbSchemaMigrator
    : IMicroServiceDbSchemaMigrator, ITransientDependency
{
    private readonly IServiceProvider _serviceProvider;

    public EntityFrameworkCoreMicroServiceDbSchemaMigrator(
        IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public async Task MigrateAsync()
    {
        /* We intentionally resolving the MicroServiceDbContext
         * from IServiceProvider (instead of directly injecting it)
         * to properly get the connection string of the current tenant in the
         * current scope.
         */

        await _serviceProvider
            .GetRequiredService<MicroServiceDbContext>()
            .Database
            .MigrateAsync();
    }
}
