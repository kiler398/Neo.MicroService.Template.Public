﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace NEO.MicroService.EntityFrameworkCore;

/* This class is needed for EF Core console commands
 * (like Add-Migration and Update-Database commands) */
public class MicroServiceDbContextFactory : IDesignTimeDbContextFactory<MicroServiceDbContext>
{

    /// <summary>
    ///  
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    /// <example>
    /// <code>
    /// 支持GIS字段.UseNetTopologySuite();
    /// </code>
    /// </example>
    public MicroServiceDbContext CreateDbContext(string[] args)
    {
        MicroServiceEfCoreEntityExtensionMappings.Configure();

        var configuration = BuildConfiguration();

        var builder = new DbContextOptionsBuilder<MicroServiceDbContext>();

        var dbContextConfigurator = new MicroServiceDbContextConfiguratorFactory().BuildDbContextConfigurator();

        dbContextConfigurator.UseDatabase(builder,
                        configuration.GetConnectionString("Default"));

        return new MicroServiceDbContext(builder.Options);
    }

    private static IConfigurationRoot BuildConfiguration()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Path.Combine(Directory.GetCurrentDirectory()))
            .AddJsonFile("design.appsettings.json", optional: true);

        return builder.Build();
    }
}
