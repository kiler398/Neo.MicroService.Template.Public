﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Neo.Abp.EntityFrameworkCore.MySQL;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace NEO.MicroService.EntityFrameworkCore;

[DependsOn(
    typeof(MicroServiceDomainModule),
    typeof(NeoAbpEntityFrameworkCoreMySQLModule)
    )]
public class MicroServiceEntityFrameworkCoreModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        MicroServiceEfCoreEntityExtensionMappings.Configure();
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAbpDbContext<MicroServiceDbContext>(options =>
        {
            /* Remove "includeAllEntities: true" to create
             * default repositories only for aggregate roots */
            options.AddDefaultRepositories(includeAllEntities: true);
        });

 
        Configure<AbpDbContextOptions>(options =>
        {
            var dbContextConfigurator = new MicroServiceDbContextConfiguratorFactory().BuildDbContextConfigurator();

            Configure<AbpDbContextOptions>(options =>
            {
                /* The main point to change your DBMS.
                 * See also TestAppMigrationsDbContextFactory for EF Core tooling. */
                dbContextConfigurator.ConfigAbpDbContextOptions(options);
            });
        });
    }
}
