﻿using Microsoft.EntityFrameworkCore;
using NEO.MicroService.Domain;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace NEO.MicroService.EntityFrameworkCore;

[ConnectionStringName("Default")]
public class MicroServiceDbContext :
    AbpDbContext<MicroServiceDbContext>, IMicroServiceDbContext 
{

    public DbSet<MicroServiceUser> Users { get; set; }
    public DbSet<MicroServiceFullUser> FullUsers { get; set; }
    public DbSet<MicroServiceMiniUser> MiniUsers { get; set; }
 



    public MicroServiceDbContext(DbContextOptions<MicroServiceDbContext> options)
        : base(options)
    {

    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);

        var dbContextConfigurator = new MicroServiceDbContextConfiguratorFactory().BuildDbContextConfigurator();

        dbContextConfigurator.EnableBatchOperation(optionsBuilder, true);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        /* Include modules to your migration db context */
 
        /* Configure your own tables/entities inside here */

        modelBuilder.ConfigureMicroService();
    }
}
