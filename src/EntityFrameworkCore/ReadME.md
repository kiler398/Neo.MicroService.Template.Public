﻿# 迁移文档

## 本地设计数据库配置

修改连接字符串的用户名密码 为 root 用户名/密码

迁移会自动建库 design_microservice

这个库仅供测试使用，如果出现无法调用Remove-migration方法的情况了，可以直接删除本地数据库。

## 配置文件修改

检查项目里面的配置的 Default 连接字符串是否正确 

## 项目设置

. 将 NEO.MicroService.EntityFrameworkCore 设置为启动项目
. VS 启动 工具-Nuget包管理工具-程序包管理控制台 设置默认项目为 NEO.MicroService.EntityFrameworkCore

## 操作命令 
 
|PMC Command|dotnet CLI command|用法|
|:-|:-:|:-:|
|add-migration <migration name>|Add <migration name>|创建迁移并添加到快照。|
|Remove-migration|Remove|删除最后一个迁移快照。|
|Update-database	|Update|基于最后一个迁移快照更新数据库。|
|Script-migration	|Script|生成所有迁移快照的脚本。|


