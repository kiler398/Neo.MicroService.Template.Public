﻿namespace NEO.MicroService;


/// <summary>
/// 错误异常码
/// </summary>
public static class MicroServiceDomainErrorCodes
{
    public const string UserNameIsEmpty = "MicroService:100001";
    public const string EmailIsExisted = "MicroService:100002";
}
