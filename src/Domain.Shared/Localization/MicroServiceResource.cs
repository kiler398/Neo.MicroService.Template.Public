﻿using Volo.Abp.Localization;

namespace NEO.MicroService.Localization;

[LocalizationResourceName("MicroService")]
public class MicroServiceResource
{

}
