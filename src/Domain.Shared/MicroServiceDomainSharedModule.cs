﻿using NEO.MicroService.Localization;
using Volo.Abp.Localization;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Modularity;
using Volo.Abp.Validation.Localization;
using Volo.Abp.VirtualFileSystem;

namespace NEO.MicroService;

[DependsOn()]
public class MicroServiceDomainSharedModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        MicroServiceGlobalFeatureConfigurator.Configure();
        MicroServiceModuleExtensionConfigurator.Configure();
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<MicroServiceDomainSharedModule>();
        });

        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Add<MicroServiceResource>("en")
                .AddVirtualJson("/Localization/MicroService")
                .AddBaseTypes(typeof(AbpValidationResource));
 
            options.DefaultResourceType = typeof(MicroServiceResource);
        });

        Configure<AbpExceptionLocalizationOptions>(options =>
        {
            options.MapCodeNamespace("MicroService", typeof(MicroServiceResource));
        });
    }
}
