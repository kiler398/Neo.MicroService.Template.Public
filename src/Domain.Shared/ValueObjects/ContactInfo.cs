﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Values;

namespace NEO.MicroService.Domain
{
    public class ContactInfo : ValueObject
    {
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }


        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Email;
        }
    }
}
