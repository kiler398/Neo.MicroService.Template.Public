﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NEO.MicroService.Const
{
    public static class MicroServicePermissionConst
    {
        public const string PermssionGroupName = "MicroService";

        public const string Permssion_MicroServiceFullUser_Create = "Permssion_MicroServiceFullUser_Create";
        public const string Permssion_MicroServiceFullUser_Update = "Permssion_MicroServiceFullUser_Update";
        public const string Permssion_MicroServiceFullUser_List = "Permssion_MicroServiceFullUser_List";
        public const string Permssion_MicroServiceFullUser_Detail = "Permssion_MicroServiceFullUser_Detail";

    }
}
