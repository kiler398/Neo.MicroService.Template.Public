﻿using Neo.Auditing;
using Neo.Security;
using System;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Timing;

namespace Neo.MicroService.EntityEventHandlers
{
    public class MicroServicesGuidKeyEntityEventHandler : BaseAuditEntityHandlers<Guid, Guid, Guid>, ITransientDependency
    {

        public MicroServicesGuidKeyEntityEventHandler(
            IClock clock,
            IAbpCurrentUser<Guid, Guid> abpCurrentUser)
            : base(clock, abpCurrentUser)
        {
        }
    }


}
