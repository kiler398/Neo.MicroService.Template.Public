using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Neo.Domain.Repositories;
using NEO.MicroService.Domain;

namespace NEO.MicroService.Repositories
{
    public interface IMicroServiceMiniUserRepository : IBaseAbpRepository<MicroServiceMiniUser, Guid>
    {
    }
}
