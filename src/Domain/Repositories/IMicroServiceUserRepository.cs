﻿using Neo.Domain.Repositories;
using NEO.MicroService.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace NEO.MicroService.Repositories
{
    public interface IMicroServiceUserRepository : IBaseAbpRepository<MicroServiceUser, Guid>
    {
    }
}
