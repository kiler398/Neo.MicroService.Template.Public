﻿using Microsoft.EntityFrameworkCore;
using Neo.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NEO.MicroService.Domain
{
    /* 根据表的实际业务需要，继承不同的Entity
     * TenantFullAuditedEntity：带租户的全审计（创建信息、修改信息、删除信息）
     * TenantCreationAuditedEntity：带租户的创建审计（创建信息）
     * TenantAuditedEntity：带租户的常用审计（创建信息、修改信息）
     * 聚合根
     * TenantCreationAuditedAggregateRoot
     * TenantAuditedAggregateRoot
     * TenantFullAuditedAggregateRoot
     * 更多Entity请查看NEO.Domain包的NEO.Domain.Entities.Auditing命名空间
     * 需要禁止表版本审计日志可以在头部加上[DisableAuditing]
     */
    [Table(MicroServiceConsts.DbTablePrefix + "Users")]
    [Comment("MicroServiceUser")]
    [Index(nameof(Name), Name = MicroServiceConsts.DbIndexPrefix + nameof(Name))]
    public class MicroServiceUser : TenantFullAuditedEntity<Guid, Guid, Guid>
    {
        /* MaxLength：确定字段长度 
         * Required：字段必填
         * Comment：数据库字段说明
         */
        [MaxLength(50)]
        [Required]
        [Comment("账户")]
        public string UserName { get; set; }

        [MaxLength(50)]
        [Comment("姓名")]
        public string Name { get; set; }

        [MaxLength(50)]
        [Comment("昵称")]
        public string Surname { get; set; }

        [MaxLength(200)]
        [Comment("邮箱")]
        public string Email { get; set; }

        /* 直接使用TypeName指定字段类型和长度（不推荐），此处只是用于演示 */
        [Column(TypeName = "varchar(200)")]
        [Comment("个人主页")]
        public string HomePage { get; set; }

        /* 直接使用TypeName指定decimal精度 */
        [Column(TypeName = "decimal(5, 2)")]
        [Comment("评分")]
        public decimal Rating { get; set; }

        [MaxLength(50)]
        [Comment("手机号")]
        public string PhoneNumber { get; set; }

        [Comment("手机号是否已确认")]
        public bool PhoneNumberConfirmed { get; set; }

        [Comment("最后登录时间")]
        public DateTime LastLoginTime { get; set; }

        /* 设置json后，EF查询可以自动序列化/反序列化，
         * 但是查询的时候json字段无法使用linq方式，需要使用EF扩展方法方式过滤json字段
         */
        [Column(TypeName = "json")]
        public ContactInfo ContactInfo { get; set; }
    }
}
