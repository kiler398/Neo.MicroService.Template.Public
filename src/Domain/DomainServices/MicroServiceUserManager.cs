﻿using NEO.MicroService.Repositories;
using System;
using NEO.MicroService.Domain;
using Neo.Domain;

namespace NEO.MicroService.DomainServices
{
    public interface IMicroServiceUserManager : IBaseDomainService<IMicroServiceUserRepository, MicroServiceUser, Guid>
    {
    }

    public class MicroServiceUserManager : BaseDomainService<IMicroServiceUserRepository, MicroServiceUser, Guid>, IMicroServiceUserManager
    {
        protected readonly IMicroServiceUserRepository _mainDomainRepository;

        public MicroServiceUserManager(IMicroServiceUserRepository mainRepository) : base(mainRepository)
        {
            _mainDomainRepository = mainRepository;
        }
    }
}
