using NEO.MicroService.Repositories;
using System;
using NEO.MicroService.Domain;
using Neo.Domain;

namespace NEO.MicroService.DomainServices
{
    public interface IMicroServiceFullUserManager : IBaseDomainService<IMicroServiceFullUserRepository, MicroServiceFullUser, Guid>
    {
    }

    public class MicroServiceFullUserManager : BaseDomainService<IMicroServiceFullUserRepository, MicroServiceFullUser, Guid>, IMicroServiceFullUserManager
    {
        protected readonly IMicroServiceFullUserRepository _mainDomainRepository;

        public MicroServiceFullUserManager(IMicroServiceFullUserRepository mainRepository) : base(mainRepository)
        {
            _mainDomainRepository = mainRepository;
        }
    }
}
