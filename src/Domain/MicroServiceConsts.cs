﻿namespace NEO.MicroService;

public static class MicroServiceConsts
{
    public const string DbTablePrefix = "MicroService_";

    public const string DbSchema = "";

    public const string DbIndexPrefix = "idx_";
}
