﻿using System.Threading.Tasks;

namespace NEO.MicroService.Data;

public interface IMicroServiceDbSchemaMigrator
{
    Task MigrateAsync();
}
