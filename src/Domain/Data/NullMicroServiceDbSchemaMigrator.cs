﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace NEO.MicroService.Data;

/* This is used if database provider does't define
 * IMicroServiceDbSchemaMigrator implementation.
 */
public class NullMicroServiceDbSchemaMigrator : IMicroServiceDbSchemaMigrator, ITransientDependency
{
    public Task MigrateAsync()
    {
        return Task.CompletedTask;
    }
}
