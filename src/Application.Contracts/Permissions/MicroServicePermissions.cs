﻿namespace NEO.MicroService.Permissions;

public static class MicroServicePermissions
{
    public const string GroupName = "MicroService";

    //Add your own permission names. Example:
    //public const string MyPermission1 = GroupName + ".MyPermission1";
}
