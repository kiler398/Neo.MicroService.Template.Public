﻿using NEO.MicroService.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace NEO.MicroService.Permissions;

public class MicroServicePermissionDefinitionProvider : PermissionDefinitionProvider
{
    /// <summary>
    /// 定义权限树
    /// </summary>
    /// <example>
    /// Define your own permissions here. Example:
    /// <code>
    /// var myGroup = context.AddGroup(MicroServicePermissions.GroupName);
    /// myGroup.AddPermission(MicroServicePermissions.MyPermission1, L("Permission:MyPermission1"));
    /// </code>
    /// </example>
    /// <param name="context"></param>
    public override void Define(IPermissionDefinitionContext context)
    {
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<MicroServiceResource>(name);
    }
}
