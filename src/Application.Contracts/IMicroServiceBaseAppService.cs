﻿using Neo.Client.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Entities;

namespace NEO.MicroService
{
    public interface IMicroServiceBaseAppService<TDto, TKey> : IApplicationService
                                                where TKey : struct
                                                where TDto : IEntityDto<TKey>
    {
        Task<TDestination> MapObjectAsync<TSource, TDestination>(TSource source);
        Task<List<TDestination>> MapListAsync<TSource, TDestination>(List<TSource> sources);

        Task<TDto> CreateAsync(TDto input);
        Task<TDto> UpdateAsync(TKey id, TDto input);
        Task DeleteByIdAsync(TKey id);
        Task<TDto> GetAsync(TKey id);
        Task InsertManyAsync(List<TDto> createInputs);
    }
}
