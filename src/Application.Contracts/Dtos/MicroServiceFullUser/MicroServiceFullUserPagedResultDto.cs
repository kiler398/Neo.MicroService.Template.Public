using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace NEO.MicroService.Dtos
{
    [Serializable]
    [SwaggerSchema(Required = new[] { "MicroServiceFullUser 分页结果数据DTO" })]
    public class MicroServiceFullUserPagedResultDto : ListResultDto<MicroServiceFullUserGetListOutputDto>, IPagedResult<MicroServiceFullUserGetListOutputDto>
    {
        [SwaggerSchema("数据总数")]
        public long TotalCount { get; set; }

        /// <summary>
        /// Creates a new <see cref="PagedResultDto{T}"/> object.
        /// </summary>
        public MicroServiceFullUserPagedResultDto()
        {

        }

        /// <summary>
        /// Creates a new <see cref="PagedResultDto{T}"/> object.
        /// </summary>
        /// <param name="totalCount">Total count of Items</param>
        /// <param name="items">List of items in current page</param>
        public MicroServiceFullUserPagedResultDto(long totalCount, IReadOnlyList<MicroServiceFullUserGetListOutputDto> items)
            : base(items)
        {
            TotalCount = totalCount;
        }
    }
}
