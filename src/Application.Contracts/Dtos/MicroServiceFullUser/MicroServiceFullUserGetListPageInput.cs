using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace NEO.MicroService.Dtos
{
    [SwaggerSchema(Required = new[] { "MicroServiceFullUser 列表分页查询 Input DTO" })]
    public class MicroServiceFullUserGetListPageInput : MicroServiceFullUserGetListInput, IPagedResultRequest
    {
        [SwaggerSchema("分页页码，从1开始")]
        public int PageIndex { get; set; }
        [SwaggerSchema("每页数据")]
        public int PageSize { get; set; }

        [JsonIgnore]
        public int SkipCount
        {
            get { return (PageIndex - 1) * PageSize; }
            set { throw new NotImplementedException(); }
        }

        [JsonIgnore]
        public int MaxResultCount
        {
            get { return PageSize; }
            set { throw new NotImplementedException(); }
        }


    }
}