using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace NEO.MicroService.Dtos
{
    [SwaggerSchema(Required = new[] { "MicroServiceFullUser 列表查询 Input DTO" })]
    public class MicroServiceFullUserGetListInput : ISortedResultRequest
    {
        [SwaggerSchema("检索名称（模糊检索）")]
        public string Keyword { get; set; }
        [SwaggerSchema("排序字段")]
        public string Sorting { get; set; }
    }
}
