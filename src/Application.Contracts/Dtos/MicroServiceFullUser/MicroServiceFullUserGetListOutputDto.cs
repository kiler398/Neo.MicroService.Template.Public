using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace NEO.MicroService.Dtos
{
    [SwaggerSchema(Required = new[] { "MicroServiceFullUser 列表数据DTO" })]
    public class MicroServiceFullUserGetListOutputDto : EntityDto<Guid>
    {
        [SwaggerSchema("用户名")]
        public string UserName { get; set; }

        [SwaggerSchema("姓名")]
        public string Name { get; set; }

        [SwaggerSchema("昵称")]
        public string Surname { get; set; }

        [SwaggerSchema("邮箱")]
        public string Email { get; set; }

        [SwaggerSchema("个人主页")]
        public string HomePage { get; set; }

        [SwaggerSchema("评分")]
        public decimal Rating { get; set; }

        [SwaggerSchema("手机号")]
        public string PhoneNumber { get; set; }

        [SwaggerSchema("手机号是否已确认")]
        public bool PhoneNumberConfirmed { get; set; }

        [SwaggerSchema("最后登录时间")]
        public DateTime LastLoginTime { get; set; }
    }
}
