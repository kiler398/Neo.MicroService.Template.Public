﻿using Volo.Abp.Modularity;
using Volo.Abp.ObjectExtending;


namespace NEO.MicroService;

[DependsOn(
    typeof(MicroServiceDomainSharedModule),
    typeof(AbpObjectExtendingModule)
)]
public class MicroServiceApplicationContractsModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        MicroServiceDtoExtensions.Configure();
    }
}
