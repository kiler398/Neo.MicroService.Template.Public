using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Neo.Client.Contracts;
using NEO.MicroService.Dtos;

namespace NEO.MicroService.Services
{
    public interface IMicroServiceUserAppService : IMicroServiceBaseAppService<MicroServiceUserDto, Guid>
    {

    }
}
