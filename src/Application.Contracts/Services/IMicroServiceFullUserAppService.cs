using Neo.Application;
using NEO.MicroService.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NEO.MicroService.Services
{
    public interface IMicroServiceFullUserAppService : IBaseCrudAppService<MicroServiceFullUserGetOutputDto,
        MicroServiceFullUserGetListOutputDto,
        MicroServiceFullUserPagedResultDto,
        Guid,
        MicroServiceFullUserGetListInput,
        MicroServiceFullUserCreateUpdateInput,
        MicroServiceFullUserCreateUpdateInput,
        MicroServiceFullUserGetListPageInput>
    {
        Task InsertManyAsync(List<MicroServiceFullUserCreateUpdateInput> createInputs);
    }
}
