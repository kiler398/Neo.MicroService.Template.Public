﻿using Microsoft.Extensions.Logging;
using NEO.MicroService.Events;
using System;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Caching;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Uow;

namespace Neo.MicroService.Handler
{
    public class TestBackgroudJob : AsyncBackgroundJob<TestInsertEvent>, ITransientDependency
    {
        private readonly ILogger<TestBackgroudJob> _logger;


        public TestBackgroudJob(ILogger<TestBackgroudJob> logger)
        {
            this._logger = logger;
        }

        public override Task ExecuteAsync(TestInsertEvent args)
        {
            this._logger.LogInformation($"开始后台任务【{args.InsertId}】操作...");

            Thread.Sleep(10000);

            this._logger.LogInformation($"后台任务【{args.InsertId}】操作结束");

            return Task.CompletedTask;
        }
    }
}
