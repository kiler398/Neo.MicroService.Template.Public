﻿using Microsoft.Extensions.Logging;
using NEO.MicroService.Events;
using System;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EventBus.Distributed;

namespace Neo.MicroService
{
    public class TestDistributedEventHandler : IDistributedEventHandler<TestDistributedEvent>, ITransientDependency
    {

        private readonly ILogger<TestDistributedEventHandler> _logger;

        public TestDistributedEventHandler(ILogger<TestDistributedEventHandler> logger)
        {
            _logger = logger;
        }


        public Task HandleEventAsync(TestDistributedEvent eventData)
        {
            _logger.LogInformation($"事件Id{eventData.EventId},事件消息：{eventData.EventMessage}");

            return Task.CompletedTask;
        }


    }
}
