﻿using Microsoft.Extensions.Logging;
using NEO.MicroService.Events;
using System;
using System.Threading;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EventBus;
using Volo.Abp.Uow;

namespace Neo.MicroService.Handler
{
    public class TestLocalEventHandler : ILocalEventHandler<TestLocalEvent>, ILocalEventHandler<TestLocalEventForAysnc>,
          ITransientDependency
    {
        private readonly ILogger<TestLocalEventHandler> _logger;

        public TestLocalEventHandler(ILogger<TestLocalEventHandler> logger)
        {
            _logger = logger;
        }


        public Task HandleEventAsync(TestLocalEvent eventData)
        {
            _logger.LogInformation($"事件Id{eventData.EventId},事件消息：{eventData.EventMessage}");

            return Task.CompletedTask;
        }

        public Task HandleEventAsync(TestLocalEventForAysnc eventData)
        {
            _logger.LogInformation($"TestLocalEventForAysnc 事件Id{eventData.EventId},事件消息：{eventData.EventMessage} 开始,{eventData.RunSeconds}秒后结束");

            Thread.Sleep(eventData.RunSeconds*1000);

            _logger.LogInformation($"TestLocalEventForAysnc 事件Id{eventData.EventId},结束");            

            return Task.CompletedTask;
        }
    }
}

