# https://hub.docker.com/_/microsoft-dotnet
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /source

# copy csproj and restore as distinct layers
COPY *.sln .
COPY ./build.ps1 .
COPY ./src ./aspnetapp/
RUN dotnet restore

# copy everything else and build app
COPY aspnetapp/. ./aspnetapp/
WORKDIR /source/aspnetapp
RUN dotnet publish -c release -o /app --no-restore

# final stage/image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY --from=build /app ./
ENTRYPOINT ["dotnet", "aspnetapp.dll"]



FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["NuGet.Config", "."]
COPY ["src/HttpApi.Host/NEO.MicroService.HttpApi.Host.csproj", "src/HttpApi.Host/"]
COPY ["src/EntityFrameworkCore/NEO.MicroService.EntityFrameworkCore.csproj", "src/EntityFrameworkCore/"]
COPY ["src/Domain/NEO.MicroService.Domain.csproj", "src/Domain/"]
COPY ["src/Domain.Shared/NEO.MicroService.Domain.Shared.csproj", "src/Domain.Shared/"]
COPY ["src/HttpApi/NEO.MicroService.HttpApi.csproj", "src/HttpApi/"]
COPY ["src/Application.Contracts/NEO.MicroService.Application.Contracts.csproj", "src/Application.Contracts/"]
COPY ["src/Application/NEO.MicroService.Application.csproj", "src/Application/"]
RUN dotnet restore "src/HttpApi.Host/NEO.MicroService.HttpApi.Host.csproj"
COPY . .
WORKDIR "/src/src/HttpApi.Host"
RUN dotnet build "NEO.MicroService.HttpApi.Host.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "NEO.MicroService.HttpApi.Host.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "NEO.MicroService.HttpApi.Host.dll"]