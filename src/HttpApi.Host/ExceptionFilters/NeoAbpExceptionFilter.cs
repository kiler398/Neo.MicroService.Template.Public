﻿
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Volo.Abp.AspNetCore.ExceptionHandling;
using Volo.Abp.DependencyInjection;
using Volo.Abp.ExceptionHandling;
using Volo.Abp.Http;
using Volo.Abp.Json;
using Microsoft.Extensions.DependencyInjection;
using System.Net;
using Volo.Abp.Threading;
using Neo.HttpApi.Host;

namespace NEO.MicroService.ExceptionFilters
{
    public class NeoAbpExceptionFilter : AbpBaseAbpExceptionFilter
    {
        protected override async Task HandleAndWrapException(ExceptionContext context)
        {
            //跳过rpc页面的错误处理
            if (context.HttpContext.Request.Path.Value.Contains("/rpc/"))
                return;

            //非api控制器不处理错误信息
            if (!context.HttpContext.Request.Path.Value.Contains("/api/"))
                return;

            await base.HandleAndWrapException(context);
        }
    }
}