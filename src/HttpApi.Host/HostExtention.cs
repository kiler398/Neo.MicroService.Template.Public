﻿using Microsoft.Extensions.Configuration;
using System;

namespace NEO.MicroService
{
    public static class HostExtention
    {
        public static IConfiguration CreateStartUpConfiguation(this string[] args)
        {
            var environmentName = "Production";

            var readEnvironmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (!string.IsNullOrEmpty(readEnvironmentName))
                environmentName = readEnvironmentName;

            return new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();

        }
    }
}
