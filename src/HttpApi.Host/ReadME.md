﻿# 项目代码格式化

需要安装Dotnet format 全局工具

`dotnet tool install -g dotnet-format`

在解决方案根路径下执行：

`dotnet format solution.sln`

# 迁移文档

## 配置文件修改

连接到生成服务器的数据库连接字符串，这块建议CI发布的时候动态修改成服务器的这个库专有的运行用户名和密码

运行用户不允许拥有添加修改数据库表结构的权限（权限也仅限于这个库），运行时严禁使用迁移操作

## 项目运行

直接运行

## 本地开发日志查看

1.本地文件日志，去项目运行目录下面的Logs目录查看日志文件

2.项目默认支持日志输出到seq，建议本地安装 seq服务。（推荐使用docker desktop的seq）。

Docker安装命令：

`docker run --name seq -d --restart unless-stopped -e ACCEPT_EULA=Y -p 5341:80 datalust/seq:latest` 



## 本地window开发环境 发布Docker镜像

如果是本地开发环境首先确保安装了Docker Desktop

可以在Powershell中输入以下命令执行

输入下面脚本执行



如果是生产环境，需要在对应的Consul服务上创建 MicroService.Production.appsettings.json 配置节

具体需要什么配置节名字可以在 Tools 控制器的 ConsulKey 方法里面获取


#生成开发测试Token
请求授权接口

{
  "name": "张三",
  "email": "san.zhang@neo.com",
  "mobilePhone": "18777828121",
  "roles": ["Admin","System"],
  "userId": "9C47E5D9-1FC3-4FF7-A629-238710E728B8",
  "tenantId": "80EBE7CA-739E-4CBF-9E51-CA41EBFB71A7",
  "tenancyName": "租户",
  "tenantName": "租户",
  "clientIp": "127.0.0.1",
  "browser": "Chorme",
  "tenancyUserId": "9C47E5D9-1FC3-4FF7-A629-238710E728B8"
}


获取到token 以后在前面加上 `Bearer ` 设置到 header 属性 Authorization 中

