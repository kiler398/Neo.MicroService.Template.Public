﻿using Volo.Abp.Caching;

namespace NEO.MicroService.Events
{

    [CacheName("CacheItem")]

    public class TestCacheItem
    {
        public string EventId { get; set; }

        public string EventMessage { get; set; }
    }
}
