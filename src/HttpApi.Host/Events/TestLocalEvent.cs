﻿using System;

namespace NEO.MicroService.Events
{
    public class TestLocalEvent
    {
        public Guid EventId { get; set; }

        public string EventMessage { get; set; }
    }
}
