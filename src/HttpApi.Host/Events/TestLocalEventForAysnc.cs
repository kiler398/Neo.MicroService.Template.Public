﻿using System;

namespace NEO.MicroService.Events
{
    public class TestLocalEventForAysnc
    {
        public Guid EventId { get; set; }

        public string EventMessage { get; set; }

        public int RunSeconds { get; set; }
    }
}
