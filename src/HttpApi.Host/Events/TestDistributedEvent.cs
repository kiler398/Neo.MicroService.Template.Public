﻿using System;
using Volo.Abp.EventBus;

namespace NEO.MicroService.Events
{
    public class TestDistributedEvent
    {
        public Guid EventId { get; set; }

        public string EventMessage { get; set; }
    }
}
