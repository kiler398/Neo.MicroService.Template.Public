﻿using System;

namespace NEO.MicroService.Events
{
    public class TestInsertEvent
    {
        public Guid InsertId { get; set; }
    }
}
