﻿using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace NEO.MicroService.Models
{
    [SwaggerSchemaFilter(typeof(IdentityUserInfoSchemaFilter))]
    public class IdentityUserInfo
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 用户登录Id
        /// </summary>
        public string Name { get; set; } = "san.zhang";
        /// <summary>
        /// 用户姓名
        /// </summary>
        public string Surname { get; set; }
        /// <summary>
        /// 用户工号
        /// </summary>
        public string UserCode { get; set; }        
        /// <summary>
        /// 用户邮件
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }
        /// <summary>
        /// 用户角色列表
        /// </summary>
        public List<string> Roles { get; set; }
        /// <summary>
        /// 租户Id
        /// </summary>
        public string TenantId { get; set; }
        /// <summary>
        /// 租户名
        /// </summary>
        public string TenantName { get; set; }
        /// <summary>
        /// 用户登录IP
        /// </summary>
        public string ClientIp { get; set; }
        /// <summary>
        /// 浏览器
        /// </summary>
        public string Browser { get; set; }

    }


    public class IdentityUserInfoSchemaFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            var roles = new OpenApiArray();
            roles.Add(new OpenApiString("Admin"));
            roles.Add(new OpenApiString("System"));

 

            schema.Example = new OpenApiObject
            {
                ["userId"] = new OpenApiString("5A34F3F1-B788-4962-BC98-126D26E5A081"),
                ["name"] = new OpenApiString("san.zhang"),
                ["surname"] = new OpenApiString("张三"),
                ["userCode"] = new OpenApiString("9527"),
                ["email"] = new OpenApiString("san.zhang@sina.com"),
                ["mobilePhone"] = new OpenApiString("18766667777"),
                ["tenantId"] = new OpenApiString("5A34F3F1-B788-4962-BC98-126D26E5A085"),
                ["tenantName"] = new OpenApiString("Neo"),
                ["clientIp"] = new OpenApiString("127.0.0.1"),
                ["browser"] = new OpenApiString("Chorme"),
                ["roles"] = roles
            };
        }
    }
}
