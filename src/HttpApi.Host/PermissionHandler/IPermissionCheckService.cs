﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NEO.MicroService.PermissionHandler
{
    public interface IPermissionCheckService
    {
        Task<PermissionCheckResult> CheckPermissions(List<string> checkPermissions);

        Task<bool> CheckPermission(string checkPermission);
    }
}
