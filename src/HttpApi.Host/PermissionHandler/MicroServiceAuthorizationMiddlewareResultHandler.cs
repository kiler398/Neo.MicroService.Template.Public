﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Policy;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Volo.Abp.Authorization;

namespace NEO.MicroService.PermissionHandler
{
    public class MicroServiceAuthorizationMiddlewareResultHandler : IAuthorizationMiddlewareResultHandler
    {

        private readonly IPermissionCheckService _permissionCheckService;

        public MicroServiceAuthorizationMiddlewareResultHandler(IPermissionCheckService permissionCheckService)
        {
            _permissionCheckService = permissionCheckService;
        }


        public async Task HandleAsync(RequestDelegate next, HttpContext context, AuthorizationPolicy policy, PolicyAuthorizationResult authorizeResult)
        {
            if (!context.User.Identity.IsAuthenticated)
            {
                context.Response.Headers["Access-Control-Allow-Origin"] = "*";
                context.Response.StatusCode = (int)HttpStatusCode.OK;
                await context.Response.WriteAsJsonAsync(new { code = 401, message = "用户验证失败,Token为空或者错误。" });
                return;
            }

            var pendingRequirements = new List<string>();
            foreach (var authorizationRequirement in policy.Requirements)
            {
                var permissionRequirement = authorizationRequirement as PermissionRequirement;

                if (permissionRequirement != null)
                    pendingRequirements.Add(((PermissionRequirement)authorizationRequirement).PermissionName);
            }

            if (pendingRequirements.Count <= 0)
            {
                await next(context);
                return;
            }

            var checkResult = await _permissionCheckService.CheckPermissions(pendingRequirements);

            if (!checkResult.Success)
            {
                context.Response.Headers["Access-Control-Allow-Origin"] = "*";
                await context.Response.WriteAsJsonAsync(new { code = 401, message = $"您暂无足够的权限执行该操作，缺少以下权限：{checkResult.CheckFailedPermissionTexts.JoinAsString(",")}" });
                return;
            }

            await next(context);
        }
    }
}
