﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using System.Linq;
using Neo.Security;
using System;
using NEO.MicroService.Const;

namespace NEO.MicroService.PermissionHandler
{
    public class MicroServicePermissionDataService : IPermissionDataService, ITransientDependency
    {
        private readonly IAbpCurrentUser<Guid, Guid> _abpCurrentUser;

        public MicroServicePermissionDataService(IAbpCurrentUser<Guid, Guid> abpCurrentUser)
        {
            _abpCurrentUser = abpCurrentUser;
        }

        public async Task<List<string>> GetCurrentUserPermissions()
        {
            var currentUserPermissions = new List<string>();

            var roles = _abpCurrentUser.Roles;

            foreach (var role in roles)
            {
                var rolePermissions = (await GetRolePermissionsByName(role)).FindAll(p => !currentUserPermissions.Contains(p));

                currentUserPermissions.AddRange(rolePermissions);
            }

            return currentUserPermissions;
        }

        /// <summary>
        /// 获取角色权限方法，建议从redis缓存读取，项目自行写实现
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public Task<List<string>> GetRolePermissionsByName(string roleName)
        {
            var currentUserPermissions = new List<string>() {
                //MicroServicePermissionConst.Permssion_MicroServiceFullUser_Create,
                MicroServicePermissionConst.Permssion_MicroServiceFullUser_Update,
                MicroServicePermissionConst.Permssion_MicroServiceFullUser_Detail,
                MicroServicePermissionConst.Permssion_MicroServiceFullUser_List
            };

            return Task.FromResult(currentUserPermissions);
        }
    }
}
