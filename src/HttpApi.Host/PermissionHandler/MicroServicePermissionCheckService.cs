﻿using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NEO.MicroService.Localization;
using Volo.Abp.DependencyInjection;

namespace NEO.MicroService.PermissionHandler
{
    public class MicroServicePermissionCheckService : IPermissionCheckService, ITransientDependency
    {
        private readonly IPermissionDataService _permissionDataService;
        private readonly IStringLocalizer<MicroServiceResource> _stringLocalizer;


        public MicroServicePermissionCheckService(
            IPermissionDataService permissionDataService,
            IStringLocalizer<MicroServiceResource> stringLocalizer
            )
        {
            _permissionDataService = permissionDataService;
            _stringLocalizer = stringLocalizer;
        }



        public async Task<bool> CheckPermission(string checkPermission)
        {
            var currentUserPermissions = await _permissionDataService.GetCurrentUserPermissions();

            return currentUserPermissions.Contains(checkPermission);
        }

        public async Task<PermissionCheckResult> CheckPermissions(List<string> checkPermissions)
        {
            var currentUserPermissions = await _permissionDataService.GetCurrentUserPermissions();
            var permissionCheckResult = new PermissionCheckResult();
            permissionCheckResult.CheckFailedPermissions = new List<string>();
            permissionCheckResult.CheckFailedPermissionTexts = new List<string>();

            foreach (var checkPermission in checkPermissions)
            {
                if (!currentUserPermissions.Contains(checkPermission))
                {
                    permissionCheckResult.CheckFailedPermissions.Add(checkPermission);
                    if (!string.IsNullOrEmpty(_stringLocalizer.GetString(checkPermission)))
                        permissionCheckResult.CheckFailedPermissionTexts.Add(_stringLocalizer.GetString(checkPermission));
                    else
                        permissionCheckResult.CheckFailedPermissionTexts.Add(checkPermission);
                }

            }

            permissionCheckResult.Success = !permissionCheckResult.CheckFailedPermissions.Any();

            return permissionCheckResult;
        }
    }
}
