﻿using System.Collections.Generic;

namespace NEO.MicroService.PermissionHandler
{
    public class PermissionCheckResult
    {
        public bool Success { get; set; }

        public List<string> CheckFailedPermissions { get; set; }

        public List<string> CheckFailedPermissionTexts { get; set; }
    }
}
