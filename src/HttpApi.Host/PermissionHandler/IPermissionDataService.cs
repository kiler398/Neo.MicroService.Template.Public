﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NEO.MicroService.PermissionHandler
{
    public interface IPermissionDataService
    {
        Task<List<string>> GetCurrentUserPermissions();

        Task<List<string>> GetRolePermissionsByName(string roleName);
    }
}
