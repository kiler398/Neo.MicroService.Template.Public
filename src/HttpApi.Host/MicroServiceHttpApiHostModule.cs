using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NEO.MicroService.EntityFrameworkCore;
using NEO.MicroService.MultiTenancy;
using StackExchange.Redis;
using Microsoft.OpenApi.Models;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.UI.MultiTenancy;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared;
using Volo.Abp.AspNetCore.Serilog;
using Volo.Abp.Autofac;
using Volo.Abp.Caching;
using Volo.Abp.Caching.StackExchangeRedis;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Swashbuckle;
using Volo.Abp.VirtualFileSystem;
using Neo.Security;
using Neo.Identity.Core;
using Neo.Jwt;
using Neo.HttpApi.Host.Host;
using Neo.HttpApi.Host;
using Neo.HttpApi;
using NEO.MicroService.Controllers;
using Neo.HttpApi.Host.Controllers;
using Volo.Abp.EventBus.RabbitMq;
using Volo.Abp.BackgroundJobs;
using NEO.MicroService.ExceptionFilters;
using Microsoft.AspNetCore.Authorization;
using NEO.MicroService.PermissionHandler;
using Neo.HttpApi.Host.Options;
using Elastic.Apm.NetCoreAll;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace NEO.MicroService;



/// <summary>
/// 微服务Host API启动模块
/// </summary>
/// <example>
/// 程序退出拦截代码
/// <code>
/// public override void OnApplicationShutdown(ApplicationShutdownContext context)
/// {
///      base.OnApplicationShutdown(context);
///  }
/// </code>
/// </code>
/// </example>
[DependsOn(
    typeof(MicroServiceHttpApiModule),
    typeof(MicroServiceApplicationModule),
    typeof(MicroServiceEntityFrameworkCoreModule),
    typeof(AbpAutofacModule),
    typeof(AbpCachingStackExchangeRedisModule),
    typeof(AbpAspNetCoreMvcUiMultiTenancyModule),
    typeof(AbpAspNetCoreSerilogModule),
    typeof(AbpSwashbuckleModule),
    typeof(AbpLocalizationModule),
    typeof(AbpCachingModule),
    typeof(AbpEventBusRabbitMqModule),
    typeof(AbpBackgroundJobsModule)
)]
public class MicroServiceHttpApiHostModule : AbpModule
{
    private const string DefaultCorsPolicyName = "Default";

    /// <summary>
    /// 启动模块服务配置
    /// </summary>
    /// <param name="context"></param>
    /// <example>
    /// 可以通过以下代码获取环境以及配置信息
    /// <code>
    /// var configuration = context.Services.GetConfiguration();
    /// var hostingEnvironment = context.Services.GetHostingEnvironment();
    /// 
    /// //配置序列号生成器
    /// context.Services.AddSingleton<ISerialNoGenerator, SerialNoGenerator>();
    /// </code>
    /// </example>
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        //配置后台任务 默认超时时间 1小时
        context.ConfigureBackgroundJob(options => { options.DefaultTimeout = 1 * 3600; });
        //配置数据加密保护
        context.ConfigureDataProtection("MicroService");
        //配置JWT服务
        context.ConfigureJwtTokenService();
        //配置身份认证模块,默认使用Guid方案
        context.SetupIdentity<Guid, Guid, AbpClaimsIdentityGuidUserGuidTenantValueExtract>(context.ReadIsConfigIdentity());
        //配置MVC默认JSON序列化方案为Json.Net
        context.ConfigureMVCNewtonsoftJson();
        //配置ABP异常拦截器，格式化api对外异常信息返回结果
        context.ConfigureABPAutoApiResponseFormat<NeoAbpExceptionFilter>();
        //健康检查配置
        context.ConfigureHealthCheck("health_check");
        //UOW配置
        context.ConfigureAbpUnitOfWorkOptions();
        //配置CSRF
        context.ConfigureAbpAntiForgeryOptions(ignoredHttpMethods: new List<string> { "POST", "PUT", "DELETE" });
        //自动API配置
        context.ConfigureConventionalControllers<MicroServiceApplicationModule>();
        //配置验证信息
        context.ConfigureAuthentication();
        //配置权限验证模块
        context.Services.AddSingleton<IAuthorizationMiddlewareResultHandler, MicroServiceAuthorizationMiddlewareResultHandler>();
        //配置国际化
        context.ConfigureLocalization(GetLanguageSetting());
        //配置分布事件，默认关闭分布式实体变更事件，防止没有RabbitMQ事务操作失败
        context.ConfigureEventBus(false);
        //配置缓存
        context.ConfigureCache("MicroService:");
        //配置跨域默认全局跨域方便开发
        context.ConfigureCors(DefaultCorsPolicyName, isGlobalCors: context.GetIsGloablCorsOrigins());
        //配置Swagger 服务信息
        context.ConfigureSwaggerServices(ConfigureSwaggerSetting());
    }


 

    /// <summary>
    /// 配置Swagger分组
    /// </summary>
    /// <returns></returns>
    private static SwaggerSetting ConfigureSwaggerSetting()
    {
        return new SwaggerSetting()
        {
            MainApiInfo = MicroServiceBaseController.ApiGroupInfo,
            GroupApiInfos = new List<ApiGroupInfo> {
                    MicroServiceBaseRpcController.ApiGroupInfo,
                    AuthorizationController.ApiGroupInfo,
                    BaseToolsController.ApiGroupInfo,
                    HostTestController.ApiGroupInfo
                },
            SupportExtentionUIs = new List<SwaggerUI>() {
                    SwaggerUI.Knife4jUI
                }
        };
    }

    /// <summary>
    /// 配置多语言设置
    /// </summary>
    /// <returns></returns>
    private static List<LanguageInfo> GetLanguageSetting()
    {
        return new List<LanguageInfo>() {
                new LanguageInfo("zh-Hans", "zh-Hans", "简体中文"),
                new LanguageInfo("en", "en", "English"),
                new LanguageInfo("zh-Hant", "zh-Hant", "繁體中文")
            };
    }


    /// <summary>
    /// 应用程序初始化代码
    /// </summary>
    /// <param name="context"></param>
    /// <example>
    /// 启用租户代码
    /// <code>
    /// if (MultiTenancyConsts.IsEnabled)
    ///     app.UseMultiTenancy();
    /// </code>
    /// 启用APM
    /// <code>
    /// var configuration = context.GetConfiguration();
    /// app.UseAllElasticApm(configuration);
    /// </code>
    /// </example>
    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();
        var env = context.GetEnvironment();
 
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        var defaultLanguage = GetLanguageSetting().FirstOrDefault(l => l.CultureName == "zh-Hans");

        app.UseRequestLocalization(GetLanguageSetting(), defaultLanguage);

        if (!env.IsDevelopment())
        {
            app.UseErrorPage();
        }

        app.UseCorrelationId();
        app.UseStaticFiles();
        app.UseRouting();
 
        app.UseAuthentication();
        app.UseAuthorization();

        app.UseCors(DefaultCorsPolicyName);

        app.UseEndpoints(endpoints =>
        {
            var serviceDiscoveryOption = context.GetConfiguration().GetOptionByName<ServiceDiscoveryOption>(ServiceDiscoveryOption.SectionName);

            endpoints.MapHealthChecks(serviceDiscoveryOption.HealthCheckPath);
        });

        app.UseAbpSerilogEnrichers();
        app.UseUnitOfWork();

        //启用SwaggerUI
        app.UseAbpNeoSwaggerUI(ConfigureSwaggerSetting());
    }
}
