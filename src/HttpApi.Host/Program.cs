﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Neo.HttpApi.Host.Options;
using Serilog;
using Serilog.Events;
using Winton.Extensions.Configuration.Consul;

namespace NEO.MicroService;

public static class Program
{
    public async static Task<int> Main(string[] args)
    {
        //初始化启动配置，读取appSetting加环境变量
        var _startUpConfiguration = args.CreateStartUpConfiguation();

        //初始化启动日志
        Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(_startUpConfiguration).CreateLogger();

        //加载服务发现配置
        var _serviceDiscoveryOption = _startUpConfiguration.GetOptionByName<ServiceDiscoveryOption>(ServiceDiscoveryOption.SectionName);

        // 获取当前应用部署名
        var appDeployName = _startUpConfiguration["App:DeployName"];

        try
        {
            Log.Information("Starting NEO.MicroService.HttpApi.Host.");
            var builder = WebApplication.CreateBuilder(args);
            builder.Host.ConfigureAppConfiguration((hostingContext, config) =>
            {
                // 加载默认配置信息到Configuration（环境变量，命令行，配置文件）
                hostingContext.Configuration = config.Build();

                // 加载Consul配置
                config.AddConsul($"{_serviceDiscoveryOption.ServiceName}.{appDeployName}.appsettings.json", options =>
                {
                    options.ConsulConfigurationOptions = cco => { cco.Address = new Uri(_serviceDiscoveryOption.ConsulUrl); }; // 1、consul地址
                    options.Optional = true; // 2、配置选项
                    options.ReloadOnChange = true; // 3、配置文件更新后重新加载
                    options.OnLoadException = exceptionContext =>
                    {
                        exceptionContext.Ignore = true;
                    }; // 4、忽略异常
                });

                //重新加入环境变量配置提高环境变量的优先级
                config.AddEnvironmentVariables();

                // 5、consul中加载的配置信息加载到Configuration对象，然后通过Configuration 对象加载项目中
                hostingContext.Configuration = config.Build();
            })
                .UseAutofac()
                .UseSerilog((hostingContext, services, loggerConfiguration) =>
                {
                    //获取程序部署版本号
                    var systemVerison = System.IO.File.ReadAllText(Path.Combine(hostingContext.HostingEnvironment.ContentRootPath, "Version.txt"));
                    //日志中加入当前服务名服务地址信息
                    //可否使用 serviceId 替代 ServiceUrl ServiceName
                    loggerConfiguration.ReadFrom.Configuration(hostingContext.Configuration)
                                        .Enrich.WithProperty("ServiceName", _serviceDiscoveryOption.ServiceName)
                                        .Enrich.WithProperty("DeployName", appDeployName)
                                        .Enrich.WithProperty("Verison", systemVerison)
                                        .Enrich.WithProperty("ServiceUrl", _serviceDiscoveryOption.ServiceUrl)
                                        .Enrich.WithProperty("ApplicationName", typeof(Program).Assembly.GetName().Name)
                                        .Enrich.WithProperty("Environment", hostingContext.HostingEnvironment.EnvironmentName);
                }
                );
            await builder.AddApplicationAsync<MicroServiceHttpApiHostModule>();
            var app = builder.Build();
            await app.InitializeApplicationAsync();
            await app.RunAsync();
            return 0;
        }
        catch (Exception ex)
        {
            Log.Fatal(ex, "Host terminated unexpectedly!");
            return 1;
        }
        finally
        {
            Log.CloseAndFlush();
        }
    }
}
