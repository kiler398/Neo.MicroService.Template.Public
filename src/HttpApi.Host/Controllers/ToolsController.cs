﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Neo.HttpApi.Host.Controllers;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NEO.MicroService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [SwaggerTag("工具API")]
    public class ToolsController : BaseToolsController
    {
        public ToolsController(
            IConfiguration configuration,
            IWebHostEnvironment webHostEnvironment,
            ILogger<ToolsController> logger) : base(configuration, webHostEnvironment, logger)
        {
        }

    }
}
