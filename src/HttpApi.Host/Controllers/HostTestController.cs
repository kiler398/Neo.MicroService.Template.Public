﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;
using NEO.MicroService.Events;
using Volo.Abp;
using Volo.Abp.Caching;
using NEO.MicroService.Localization;
using Volo.Abp.EventBus.Local;
using Volo.Abp.BackgroundJobs;
using Neo.HttpApi;
using NEO.MicroService;
using Microsoft.Extensions.Logging;
using Volo.Abp.EventBus.Distributed;
using Microsoft.Extensions.Configuration;

namespace NEO.MicroService.Controllers
{
    /// <summary>
    /// 测试控制器
    /// </summary>
    /// <example>
    /// <code>
    /// private readonly ISerialNoGenerator _serialNoGenerator;
    /// 
    /// , ISerialNoGenerator serialNoGenerator
    /// 
    /// _serialNoGenerator = serialNoGenerator;
    /// 
    /// public override void OnApplicationShutdown(ApplicationShutdownContext context)
    /// {
    ///      base.OnApplicationShutdown(context);
    /// }
    /// 
    /// 
    /// [HttpGet("TestSerialNoGenerator")]
    /// [SwaggerOperation(
    ///     Summary = "测试生成序列号",
    ///     Description = "",
    ///     OperationId = "TestSerialNoGenerator",
    ///     Tags = new[] { "HostTest" }
    /// )]
    /// public async Task<string> TestSerialNoGenerator()
    /// {
    ///    return await _serialNoGenerator.GeneratorNo("Test", 5, '|');
    /// }
    /// </code>
    /// </example>
    [Route("api/[controller]")]
    [ApiController]
    [SwaggerTag(TagName)]
    [ApiExplorerSettings(GroupName = APIGroupName)]
    public class HostTestController : MicroServiceBaseController
    {
        protected const string TagName = "HostTest";

        public new const string APIGroupName = "test";

        private static ApiGroupInfo _apiGroupInfo = new ApiGroupInfo()
        {
            VersionRouterPath = APIGroupName,
            Title = "平台测试 API文档",
            Version = "test",
            Description = "NEO 平台测试 API文档"
        };

        public new static ApiGroupInfo ApiGroupInfo
        {
            get
            {
                return _apiGroupInfo;
            }
        }


        private readonly IConfiguration _configuration;
        private readonly ILogger<HostTestController> _logger;
        private readonly IDistributedCache<TestCacheItem> _distributedCacheItem;
        private readonly ILocalEventBus _localEventBus;
        private readonly IDistributedEventBus _distributedEventBus;
        private readonly IStringLocalizer<MicroServiceResource> _stringLocalizer;
        private readonly IBackgroundJobManager _backgroundJobManager;



        public HostTestController(
            ILogger<HostTestController> logger
            , IDistributedCache<TestCacheItem> distributedCacheItem
            , IStringLocalizer<MicroServiceResource> stringLocalizer
            , ILocalEventBus localEventBus
            , IDistributedEventBus distributedEventBus
            , IBackgroundJobManager backgroundJobManager
            , IConfiguration configuration
            ) : base()
        {
            _logger = logger;
            _distributedCacheItem = distributedCacheItem;
            _stringLocalizer = stringLocalizer;
            _localEventBus = localEventBus;
            _distributedEventBus = distributedEventBus;
            _backgroundJobManager = backgroundJobManager;
            _configuration = configuration;
        }


        [HttpPost("TestLocalEventBus")]
        [SwaggerOperation(
            Summary = "测试内存事件总线-阻塞当前请求方式",
            Description = "测试内存事件总线-阻塞当前请求方式",
            Tags = new[] { TagName }
        )]
        public async Task<bool> TestLocalEventBus()
        {
            var insertId = Guid.NewGuid();

            await _localEventBus.PublishAsync(new TestLocalEvent() { EventId = insertId, EventMessage = "测试内存事件总线" });

            return true;
        }


        [HttpPost("TestLocalEventBus2")]
                [SwaggerOperation(
            Summary = "测试内存事件总线-不阻塞当前请求方式",
            Description = "测试内存事件总线-不阻塞当前请求方式",
            Tags = new[] { TagName }
        )]
        public async Task<bool> TestLocalEventBus2(int event1ExcuteDelaySeconds = 10,int event2ExcuteDelaySeconds =5)
        {
            var methodId = Guid.NewGuid();

            _logger.LogInformation($"TestLocalEventBus2 {methodId} 事件开始");

            var insertId = Guid.NewGuid();

            Task.Run(() =>
            {
                return _localEventBus.PublishAsync(new TestLocalEventForAysnc() { EventId = insertId, EventMessage = "测试内存事件总线",RunSeconds = event1ExcuteDelaySeconds });
            });

            var insertId2 = Guid.NewGuid();

            Task.Run(() =>
            {
                return _localEventBus.PublishAsync(new TestLocalEventForAysnc() { EventId = insertId2, EventMessage = "测试内存事件总线", RunSeconds = event2ExcuteDelaySeconds });
            });

            _logger.LogInformation($"TestLocalEventBus2 {methodId} 事件结束，发布事件 {insertId} {insertId2}");

            return true;

        }


        [HttpPost("TestDistributedEventBus")]
        [SwaggerOperation(
            Summary = "测试分布式事件总线",
            Description = "测试分布式事件总线",
            Tags = new[] { TagName }
        )]
        public async Task<bool> TestDistributedEventBus()
        {
            var insertId = Guid.NewGuid();

            await _distributedEventBus.PublishAsync(new TestDistributedEvent() { EventId = insertId, EventMessage = "测试分布式事件总线" });

            return true;

        }        


        [HttpPost("TestLogger")]
        [SwaggerOperation(
            Summary = "测试日志",
            Description = "测试日志",
            Tags = new[] { TagName }
        )]
        public Task<string> TestLogger(
            [FromBody]
            string txtMessage)
        {
            _logger.LogInformation(txtMessage);

            return Task.FromResult(txtMessage);
        }


        [HttpPost("TestConfig")]
        [SwaggerOperation(
            Summary = "测试配置",
            Description = "测试配置",
            Tags = new[] { TagName }
        )]
        public Task<string> TestConfig(
    [FromBody]
            string txtConfigName)
        {
            return Task.FromResult(_configuration[txtConfigName]);
        }


        [HttpPost("TestLongTimeTaskByBackgroundJob")]
        [SwaggerOperation(
            Summary = "通过后台任务执行长时间任务",
            Description = "通过后台任务执行长时间任务",
            Tags = new[] { TagName }
        )]
        public async Task<bool> TestLongTimeTaskByBackgroundJob()
        {
            var insertId = Guid.NewGuid();

            await _backgroundJobManager.EnqueueAsync(new TestInsertEvent() { InsertId = insertId });

            return true;

        }

        [HttpPost("TestLocalizer")]
        [SwaggerOperation(
            Summary = "测试本地化文本",
            Description = "测试本地化文本",
            Tags = new[] { TagName }
        )]
        public Task<string> TestLocalizer(
            [SwaggerRequestBody("测试本地化文本", Required = true)]
                        [FromBody]
                        string textCode
        )
        {
            return Task.FromResult<string>(_stringLocalizer[textCode]);
        }


        [HttpPost("TestCache1")]
        [SwaggerOperation(
            Summary = "测试缓存2",
            Description = "",
            Tags = new[] { TagName }
        )]
        public async Task<TestCacheItem> TestCache1(
        [SwaggerRequestBody("测试缓存2", Required = true)]
                                [FromBody]
                                TestCacheItem cacheItem
        )
        {
            var cacheId = Guid.NewGuid().ToString();

            await _distributedCacheItem.SetAsync(cacheId, cacheItem);

            var cacheItem2 = await _distributedCacheItem.GetAsync(cacheId);

            return cacheItem2;
        }

        [HttpGet("TestException1")]
        [SwaggerOperation(
            Summary = "测试自定义异常",
            Description = "",
            Tags = new[] { TagName }
        )]
        public void TestException1(
)
        {
            throw new BusinessException(MicroServiceDomainErrorCodes.UserNameIsEmpty);
        }


        [HttpGet("TestException2")]
        [SwaggerOperation(
            Summary = "测试自定义异常2",
            Description = "",
            Tags = new[] { TagName }
        )]
        public void TestException2(
)
        {
            throw new UserFriendlyException(message: L[MicroServiceDomainErrorCodes.UserNameIsEmpty], MicroServiceDomainErrorCodes.UserNameIsEmpty);
        }

        [HttpPost("TestExceptionCode")]
        [SwaggerOperation(
            Summary = "测试自定义异常",
            Description = "",
            Tags = new[] { TagName }
        )]
        public void TestExceptionCode(
        [FromQuery, SwaggerParameter("自定义异常编码", Required = true)]
                string errorCode
        )
        {
            throw new BusinessException(errorCode);
        }


    }


}
