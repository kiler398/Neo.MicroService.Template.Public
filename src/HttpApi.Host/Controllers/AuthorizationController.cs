﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Neo.HttpApi;
using Neo.HttpApi.Models;
using Neo.Jwt;
using Neo.Security;
using NEO.MicroService.Models;
using NEO.MicroService.PermissionHandler;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using NEO.MicroService.Controllers;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NEO.MicroService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [SwaggerTag("授权API")]
    [ApiExplorerSettings(GroupName = APIGroupName)]
    public class AuthorizationController : MicroServiceBaseController
    {
        public new const string APIGroupName = "auth";

        private static readonly ApiGroupInfo _apiGroupInfo = new ApiGroupInfo()
        {
            VersionRouterPath = APIGroupName,
            Title = "平台授权 API文档",
            Version = "auth",
            Description = "NEO 平台授权 API文档"
        };

        public new static ApiGroupInfo ApiGroupInfo
        {
            get
            {
                return _apiGroupInfo;
            }
        }

        private readonly IPermissionDataService _permissionDataService;
        private readonly ITokenService _tokenService;
        private readonly IAbpCurrentUser<Guid, Guid> _currentUser;

        public AuthorizationController(
         ITokenService tokenService,
         IAbpCurrentUser<Guid, Guid> currentUser,
         IPermissionDataService permissionDataService
            ) : base()
        {

            _currentUser = currentUser;
            _tokenService = tokenService;
            _permissionDataService = permissionDataService;
        }

        /// <summary>
        /// 生成测试Token
        /// </summary>
        /// <example>
        /// 使用下面的json创建用户
        /// <code>
        /// {
        /// "name": "san.zhang",
        /// "email": "san.zhang@163.com",
        /// "mobilePhone": "18766668888",
        /// "roles": [ "Admin", "System" ],
        /// "userId": "109891",
        /// "tenantId": "1",
        /// "tenancyName": "DUXACT",
        /// "tenantName": "DUXACT",
        /// "clientIp": "127.0.0.1",
        /// "browser": "Chorme",
        /// "tenancyUserId": "109891"
        /// }
        /// </code>
        /// </example>
        /// <param name="identityUser"></param>
        /// <returns></returns>
        [HttpPost("CreatToken")]
        [SwaggerOperation(
            Summary = "创建用户登录Token",
            Description = @"创建用户登录Token,Authorization Bearer {token}",
            OperationId = "CreatToken",
            Tags = new[] { "授权API" }
        )]
        [AllowAnonymous]
        public string CreatToken([FromBody] IdentityUserInfo identityUser)
        {
            var claims = new List<Claim>();

            claims.Add(new Claim(NeoAbpClaimTypes.UserName, identityUser.Name));

            claims.Add(new Claim(NeoAbpClaimTypes.Surname, identityUser.Surname));

            claims.Add(new Claim(NeoAbpClaimTypes.UserId, identityUser.UserId));

            claims.Add(new Claim(NeoAbpClaimTypes.UserCode, identityUser.UserCode));

            claims.Add(new Claim(NeoAbpClaimTypes.Email, identityUser.Email));

            claims.Add(new Claim(NeoAbpClaimTypes.MobilePhone, identityUser.MobilePhone));

            claims.Add(new Claim(NeoAbpClaimTypes.TenantId, identityUser.TenantId.ToString()));

            claims.Add(new Claim(NeoAbpClaimTypes.TenantName, identityUser.TenantName));

            foreach (var role in identityUser.Roles)
            {
                claims.Add(new Claim(NeoAbpClaimTypes.Role, role));
            }

            return _tokenService.GenerateAccessToken(claims.ToArray());
        }


        [HttpGet("GetCurrentUserInfo")]
        [SwaggerOperation(
            Summary = "获取当前登录用户身份信息",
            Description = "获取当前登录用户身份信息",
            OperationId = "GetCurrentUserInfo",
            Tags = new[] { "授权API" }
        )]
        [Authorize]
        public IAbpCurrentUser<Guid, Guid> GetCurrentUserInfo()
        {
            var identity = _currentUser;

            return identity;
        }

        [HttpGet("GetCurrentUserPermissions")]
        [SwaggerOperation(
            Summary = "获取当前登录用户所有权限码",
            Description = "获取当前登录用户所有权限码",
            OperationId = "GetCurrentUserPermissions",
            Tags = new[] { "授权API" }
        )]
        [Authorize]
        public async Task<List<string>> GetCurrentUserPermissions()
        {
            return await _permissionDataService.GetCurrentUserPermissions();
        }

        [HttpGet("TestPermission")]
        [SwaggerOperation(
            Summary = "测试当前用户是否拥有权限（有权限）",
            Description = "测试当前用户是否拥有权限（有权限）",
            OperationId = "TestPermission",
            Tags = new[] { "授权API" }
        )]
        [Authorize("MicroService_MicroServiceFullUser_Create")]
        public async Task<string> TestPermission()
        {
            await TestAction("TestPermission");
            return "OK";
        }

        [HttpGet("TestNotPermission")]
        [SwaggerOperation(
            Summary = "测试当前用户是否拥有权限（无权限）",
            Description = "测试当前用户是否拥有权限（无权限）",
            OperationId = "TestNotPermission",
            Tags = new[] { "授权API" }
        )]
        [Authorize("MicroService_MicroServiceFullUser_Create1")]
        public async Task<string> TestNotPermission()
        {
            await TestAction("TestNotPermission");
            return "OK";
        }

        private static Task TestAction(string actionName)
        {
            Console.WriteLine(actionName);
            return Task.CompletedTask;
        }

    }
}
