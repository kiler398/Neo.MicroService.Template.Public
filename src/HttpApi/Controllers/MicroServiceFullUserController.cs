using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Uow;
using Swashbuckle.AspNetCore.Annotations;
using NEO.MicroService.Services;
using Neo.HttpApi.Models;
using NEO.MicroService.Dtos;

namespace NEO.MicroService.Controllers
{

    /// <summary>
    /// 数据访问api控制器
    /// </summary>
    /// <example>
    /// 隐藏API
    /// 头部加上标注
    /// <code>
    /// [ApiExplorerSettings(IgnoreApi = true)]
    /// </code>
    /// </example>
    [UnitOfWork]
    [Route("api/MicroService/[controller]")]
    [ApiController]
    [SwaggerTag(TagName)]
    public class MicroServiceFullUserController : MicroServiceBaseController
    {
        protected const string TagName = "MicroServiceFullUser";

        private readonly IMicroServiceFullUserAppService _microServiceUserAppService;

        /// <summary>
        /// 控制器构造函数
        /// </summary>
        /// <param name="microServiceUserAppService"></param>
        /// <example>
        /// 如果需要使用日志和配置需要注入aspnet core 统一配置和日志接口
        /// <code>
        /// private readonly IConfiguration _configuration;
        /// private readonly ILogger<MicroServiceFullUserController> _logger;
        /// 
        /// public MicroServiceFullUserController(
        ///     IMicroServiceFullUserAppService microServiceUserAppService,
        ///     IConfiguration configuration,
        ///     ILogger<MicroServiceFullUserController> logger) : base()
        /// {
        ///     _microServiceUserAppService = microServiceUserAppService;
        ///     _configuration = configuration;
        ///     _logger = logger;
        /// }
        /// 
        /// </code>
        /// </example>
        public MicroServiceFullUserController(
            IMicroServiceFullUserAppService microServiceUserAppService) : base()
        {
            _microServiceUserAppService = microServiceUserAppService;
        }


        /// <summary>
        /// /
        /// </summary>
        /// <param name="createInput"></param>
        /// <example>
        /// <code>
        /// 设置权限 
        /// [Authorize("MicroService_MicroServiceFullUser_Create")]
        /// </code>
        /// </example>
        /// <returns></returns>
        [HttpPost("")]
        [SwaggerOperation(
            Summary = "新建数据",
            Description = "新建数据",
            Tags = new[] { TagName }
        )]
        public async Task<MicroServiceFullUserGetOutputDto> Create(
                [SwaggerRequestBody("创建数据", Required = true)]
                [FromBody]
                MicroServiceFullUserCreateUpdateInput createInput
            )
        {
            var result = await _microServiceUserAppService.CreateAsync(createInput);

            return result;
        }

        [HttpPost("{id}/Update")]
        [SwaggerOperation(
            Summary = "更新数据",
            Description = "更新数据",
            Tags = new[] { TagName }
        )]
        public async Task<MicroServiceFullUserGetOutputDto> Update(
                [SwaggerParameter("更新数据Id", Required = true)]
                Guid id,
                [SwaggerRequestBody("更新数据", Required = true)]
                [FromBody]
                MicroServiceFullUserCreateUpdateInput updateInput
            )
        {
            var result = await _microServiceUserAppService.UpdateAsync(id, updateInput);

            return result;
        }

        [HttpPost("{id}/Delete")]
        [SwaggerOperation(
            Summary = "删除数据",
            Description = "删除数据",
            Tags = new[] { TagName }
        )]
        public async Task<dynamic> Delete(
                [SwaggerParameter("删除数据Id", Required = true)]
                Guid id
            )
        {
            await _microServiceUserAppService.DeleteAsync(id);

            var deleteId = new { id = id };

            return deleteId;
        }

        [HttpPost("Query")]
        [SwaggerOperation(
            Summary = "查询数据列表",
            Description = "查询数据列表",
            Tags = new[] { TagName }
        )]
        public async Task<List<MicroServiceFullUserGetListOutputDto>> Query(
                [SwaggerRequestBody("查询条件", Required = true)]
                [FromBody] MicroServiceFullUserGetListInput getListInput
            )
        {
            var result = await _microServiceUserAppService.Query(getListInput);

            return result;
        }

        [HttpPost("QueryCount")]
        [SwaggerOperation(
            Summary = "查询总数",
            Description = "查询总数",
            Tags = new[] { TagName }
        )]
        public async Task<long> QueryCount(
                [SwaggerRequestBody("查询条件", Required = true)]
                [FromBody] MicroServiceFullUserGetListInput getListInput
            )
        {
            var result = await _microServiceUserAppService.QueryCount(getListInput);

            return result;
        }


        [HttpPost("PageQuery")]
        [SwaggerOperation(
            Summary = "分页查询",
            Description = "分页查询",
            Tags = new[] { TagName }
        )]
        public async Task<MicroServiceFullUserPagedResultDto> PageQuery(
            [SwaggerRequestBody("查询条件", Required = true)]
            [FromBody] MicroServiceFullUserGetListPageInput getListPageInput
            )
        {
            var result = await _microServiceUserAppService.QueryByPage(getListPageInput);

            return result;
        }

        [HttpGet("{id}")]
        [SwaggerOperation(
            Summary = "通过ID获取详细信息",
            Description = "通过ID获取详细信息",
            Tags = new[] { TagName }
        )]
        public async Task<MicroServiceFullUserGetOutputDto> Get([SwaggerParameter("查询数据Id", Required = true)] Guid id)
        {
            var result = await _microServiceUserAppService.GetAsync(id);

            return result;
        }




    }
}
