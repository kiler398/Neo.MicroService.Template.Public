﻿using Neo.Controllers;
using Neo.HttpApi;
using NEO.MicroService.Localization;

namespace NEO.MicroService.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class MicroServiceBaseRpcController : BaseController
    {
        public const string APIGroupName = "rpcapi";

        private static ApiGroupInfo _apiGroupInfo = new ApiGroupInfo()
        {
            VersionRouterPath = APIGroupName,
            Title = "MicroService RPC远程API文档",
            Version = "rpcapi",
            Description = "NEO MicroService RPC远程API文档"
        };

        public static ApiGroupInfo ApiGroupInfo
        {
            get
            {
                return _apiGroupInfo;
            }
        }
        protected MicroServiceBaseRpcController()
        {
            LocalizationResource = typeof(MicroServiceResource);
        }
    }
}