﻿using Localization.Resources.AbpUi;
using NEO.MicroService.Localization;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
 
namespace NEO.MicroService;

[DependsOn(
    typeof(MicroServiceApplicationContractsModule)
    )]
public class MicroServiceHttpApiModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        ConfigureLocalization();
    }

    private void ConfigureLocalization()
    {
        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Get<MicroServiceResource>()
                .AddBaseTypes(
                    typeof(AbpUiResource)
                );
        });
    }
}
