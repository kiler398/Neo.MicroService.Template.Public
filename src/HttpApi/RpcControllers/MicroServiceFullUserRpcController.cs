using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;
using NEO.MicroService.Controllers;
using Volo.Abp.Uow;
using Neo.Abp.AutoReponseWarpper.Attributes;

namespace NEO.MicroService.RpcControllers
{
    /// <summary>
    /// 数据访问api控制器
    /// </summary>
    /// <example>
    /// 隐藏API
    /// 头部加上标注
    /// <code>
    /// [ApiExplorerSettings(IgnoreApi = true)]
    /// </code>
    /// </example>
    [UnitOfWork]
    [Route("rpc/MicroService/[controller]")]
    [ApiController]
    [SwaggerTag("MicroServiceFullUserRpc")]
    [ApiExplorerSettings(GroupName = APIGroupName)]
    [NoAutoWrapper]
    public class MicroServiceFullUserRpcController : MicroServiceBaseRpcController
    {

        private readonly ILogger<MicroServiceFullUserController> _logger;

        /// <summary>
        /// 控制器构造函数
        /// </summary>
        /// <param name="microServiceUserAppService"></param>
        /// <example>
        /// 如果需要使用日志和配置需要注入aspnet core 统一配置和日志接口
        /// <code>
        /// private readonly IConfiguration _configuration;
        /// private readonly ILogger<MicroServiceFullUserController> _logger;
        /// private readonly IMicroServiceFullUserAppService _microServiceUserAppService;
        /// 
        /// public MicroServiceFullUserRpcController(
        ///     IMicroServiceFullUserAppService microServiceUserAppService,
        ///     IConfiguration configuration,
        ///     ILogger<MicroServiceFullUserController> logger) : base()
        /// {
        ///     _microServiceUserAppService = microServiceUserAppService;
        ///     _configuration = configuration;
        ///     _logger = logger;
        /// }
        /// </code>
        /// </example>
        public MicroServiceFullUserRpcController(ILogger<MicroServiceFullUserController> logger) : base()
        {
            _logger = logger;
        }

        [HttpPost("TestRemoteRpc")]
        [SwaggerOperation(
            Summary = "远程接口测试",
            Description = "远程接口测试，返回一个Guid",
            OperationId = "MicroServiceFullUserRpc_TestRemoteRpc",
            Tags = new[] { "MicroServiceFullUserRpc" }
        )]
        public Task<Guid> TestRemoteRpc()
        {
            _logger.LogInformation("调用远程测试接口");
            return Task.FromResult(Guid.NewGuid());
        }
    }
}
