﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Neo.Abp.AutoReponseWarpper.Filter;
using Neo.Controllers;
using Neo.HttpApi;
using NEO.MicroService.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;

namespace NEO.MicroService.Controllers
{
    /* Inherit your controllers from this class.
     */
    [AutoApiResultFilter]
    public abstract class MicroServiceBaseController : BaseController
    {
        public const string APIGroupName = "v1";

        private static ApiGroupInfo _apiGroupInfo = new ApiGroupInfo()
        {
            VersionRouterPath = APIGroupName,
            Title = "MicroService API文档",
            Version = "v1",
            Description = "NEO MicroService API文档"
        };

        public static ApiGroupInfo ApiGroupInfo
        {
            get
            {
                return _apiGroupInfo;
            }
        }
        protected MicroServiceBaseController()
        {
            LocalizationResource = typeof(MicroServiceResource);
        }
    }
}