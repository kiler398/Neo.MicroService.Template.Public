using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Validation;
using Volo.Abp;
using NEO.MicroService.Repositories;
using System.Linq.Expressions;
using NEO.MicroService.DomainServices;
using NEO.MicroService.Domain;
using NEO.MicroService.Dtos;

namespace NEO.MicroService.Services
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = false)]
    public class MicroServiceUserAppService : MicroServiceBaseAppService<MicroServiceUser,
                                                                             MicroServiceUserDto,
                                                                             Guid,
                                                                             IMicroServiceUserRepository,
                                                                             IMicroServiceUserManager>, IMicroServiceUserAppService
    {
        public MicroServiceUserAppService(
            IMicroServiceUserManager mainManager
            ) : base(mainManager)
        {
        }
    }
}
