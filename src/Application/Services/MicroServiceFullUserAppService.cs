using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Validation;
using Volo.Abp;
using NEO.MicroService.Repositories;
using System.Linq.Expressions;
using NEO.MicroService.DomainServices;
using NEO.MicroService.Domain;
using NEO.MicroService.Dtos;
using NEO.MicroService.Services;

namespace NEO.MicroService.Services
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = false)]
    public class MicroServiceFullUserAppService : MicroServiceBaseCurdAppService<MicroServiceFullUser,
                                                                             MicroServiceFullUserGetOutputDto,
                                                                             MicroServiceFullUserGetListOutputDto,
                                                                             MicroServiceFullUserPagedResultDto,
                                                                             Guid,
                                                                             MicroServiceFullUserGetListInput,
                                                                             MicroServiceFullUserCreateUpdateInput,
                                                                             MicroServiceFullUserCreateUpdateInput,
                                                                             MicroServiceFullUserGetListPageInput,
                                                                             IMicroServiceFullUserRepository,
                                                                             IMicroServiceFullUserManager>,
                                               IMicroServiceFullUserAppService
    {
        public MicroServiceFullUserAppService(
            IMicroServiceFullUserManager mainManager,
            IMicroServiceFullUserRepository mainRepository
            ) : base(mainRepository, mainManager)
        {
        }
    }
}
