﻿using Neo.Domain;
using Neo.Domain.Repositories;
using Neo.Client.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;

namespace NEO.MicroService
{
    public class MicroServiceBaseAppService<TEntity, TDto, TKey, TRepository, TManage> :
                                               ApplicationService,
                                               IMicroServiceBaseAppService<TDto, TKey>
                                                where TKey : struct
                                                where TEntity : class, IEntity<TKey>
                                                where TDto : IEntityDto<TKey>
                                                where TRepository : IBaseAbpRepository<TEntity, TKey>
                                                where TManage : IBaseDomainService<TRepository, TEntity, TKey>
    {

        protected readonly TManage _mainManager;

        public MicroServiceBaseAppService(
            TManage mainManager
        )
        {
            _mainManager = mainManager;
        }

        /// <summary>
        /// Map 单个对象
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public virtual Task<TDestination> MapObjectAsync<TSource, TDestination>(TSource source)
        {
            return Task.FromResult(ObjectMapper.Map<TSource, TDestination>(source));
        }

        /// <summary>
        /// Map List 对象
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="sources"></param>
        /// <returns></returns>
        public virtual Task<List<TDestination>> MapListAsync<TSource, TDestination>(List<TSource> sources)
        {
            return Task.FromResult(ObjectMapper.Map<List<TSource>, List<TDestination>>(sources));
        }

        protected virtual Task<TEntity> MapToEntityAsync(TDto dto)
        {
            return MapObjectAsync<TDto, TEntity>(dto);
        }

        protected virtual Task<TDto> MapToDtoAsync(TEntity entity)
        {
            return MapObjectAsync<TEntity, TDto>(entity);
        }

        protected virtual Task<List<TEntity>> MapToEntityListAsync(List<TDto> dtos)
        {
            return MapListAsync<TDto, TEntity>(dtos);
        }

        protected virtual Task<List<TDto>> MapToDtoListAsync(List<TEntity> entities)
        {
            return MapListAsync<TEntity, TDto>(entities);
        }

        public virtual async Task<TDto> CreateAsync(TDto input)
        {
            var entity = await MapToEntityAsync(input);

            await _mainManager.InsertAsync(entity, true);

            return await MapToDtoAsync(entity);
        }

        public virtual async Task<TDto> UpdateAsync(TKey id, TDto input)
        {
            var updateEntity = await _mainManager.GetAsync(id);

            await _mainManager.UpdateAsync(updateEntity);

            return await MapToDtoAsync(updateEntity);
        }

        protected virtual async Task DeleteAsync(TEntity entity)
        {
            await _mainManager.DeleteAsync(entity);
        }

        public virtual async Task DeleteByIdAsync(TKey id)
        {
            await _mainManager.DeleteByIdAsync(id);
        }

        public virtual async Task<TDto> GetAsync(TKey id)
        {
            return await MapToDtoAsync(await _mainManager.GetAsync(id));
        }

        public async Task InsertManyAsync(List<TDto> createInputs)
        {
            var entities = await MapListAsync<TDto, TEntity>(createInputs);
            await this._mainManager.InsertManyAsync(entities);
        }

    }
}