﻿using System;
using System.Collections.Generic;
using System.Text;
using NEO.MicroService.Localization;
using Volo.Abp.Application.Services;

namespace NEO.MicroService;

/* Inherit your application services from this class.
 */
public abstract class MicroServiceAppService : ApplicationService
{
    protected MicroServiceAppService()
    {
        LocalizationResource = typeof(MicroServiceResource);
    }
}
