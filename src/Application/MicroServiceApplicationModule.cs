﻿using Volo.Abp.AutoMapper;
using Volo.Abp.Json;
using Volo.Abp.Modularity;

namespace NEO.MicroService;

[DependsOn(
    typeof(MicroServiceDomainModule),
    typeof(MicroServiceApplicationContractsModule),
    typeof(AbpJsonModule),
    typeof(AbpAutoMapperModule)
    )]
public class MicroServiceApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddMaps<MicroServiceApplicationModule>();
        });

        Configure<AbpJsonOptions>(options =>
        {
            options.UseHybridSerializer = false;
        });
    }
}
