﻿using AutoMapper;

namespace NEO.MicroService;

public class MicroServiceApplicationAutoMapperProfile : Profile
{
    public MicroServiceApplicationAutoMapperProfile()
    {
        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */
    }
}
