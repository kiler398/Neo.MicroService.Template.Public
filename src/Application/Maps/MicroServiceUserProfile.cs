using AutoMapper;
using NEO.MicroService.Domain;
using NEO.MicroService.Dtos;

namespace NEO.MicroService.Maps
{
    public class MicroServiceUserProfile : Profile
    {
        /// <summary>
        /// You can configure your AutoMapper mapping configuration here.
        /// Alternatively, you can split your mapping configurations
        /// into multiple profile classes for a better organization.
        /// </summary>
        /// <example>
        /// Mapper code example:
        /// <code>
        /// CreateMap<MicroServiceUser, MicroServiceUserDto>();
        /// CreateMap<MicroServiceUser, MicroServiceUserDto>().IgnoreAuditedObjectProperties();
        /// CreateMap<MicroServiceUserDto, MicroServiceUser>();
        /// CreateMap<MicroServiceUserDto, MicroServiceUser>().IgnoreAuditedObjectProperties();
        /// </code>
        /// </example>
        public MicroServiceUserProfile()
        {
            CreateMap<MicroServiceUser, MicroServiceUserDto>();
            CreateMap<MicroServiceUserDto, MicroServiceUser>();
        }
    }
}
