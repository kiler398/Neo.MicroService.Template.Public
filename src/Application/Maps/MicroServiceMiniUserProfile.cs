using AutoMapper;
using NEO.MicroService.Domain;
using NEO.MicroService.Dtos;

namespace NEO.MicroService.Maps
{
    public class MicroServiceMiniUserProfile : Profile
    {
        /// <summary>
        /// You can configure your AutoMapper mapping configuration here.
        /// Alternatively, you can split your mapping configurations
        /// into multiple profile classes for a better organization.
        /// </summary>
        /// <example>
        /// Mapper code example:
        /// <code>
        /// CreateMap<MicroServiceMiniUser, MicroServiceMiniUserDto>();
        /// CreateMap<MicroServiceMiniUser, MicroServiceMiniUserDto>().IgnoreAuditedObjectProperties();
        /// CreateMap<MicroServiceMiniUserDto, MicroServiceMiniUser>();
        /// CreateMap<MicroServiceMiniUserDto, MicroServiceMiniUser>().IgnoreAuditedObjectProperties();
        /// </code>
        /// </example>
        public MicroServiceMiniUserProfile()
        {
            CreateMap<MicroServiceMiniUser, MicroServiceMiniUserDto>();
            CreateMap<MicroServiceMiniUserDto, MicroServiceMiniUser>();
        }
    }
}
