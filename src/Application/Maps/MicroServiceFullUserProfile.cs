using AutoMapper;
using NEO.MicroService.Domain;
using NEO.MicroService.Dtos;

namespace NEO.MicroService.Maps
{
    public class MicroServiceFullUserProfile : Profile
    {
        /// <summary>
        /// You can configure your AutoMapper mapping configuration here.
        /// Alternatively, you can split your mapping configurations
        /// into multiple profile classes for a better organization.
        /// </summary>
        /// <example>
        /// Mapper code example:
        /// <code>
        /// CreateMap<MicroServiceFullUser, MicroServiceFullUserGetOutputDto>();
        /// CreateMap<MicroServiceFullUser, MicroServiceFullUserGetListOutputDto>();
        /// CreateMap<MicroServiceFullUserGetOutputDto, MicroServiceFullUser>();
        /// CreateMap<MicroServiceFullUserDto, MicroServiceFullUser>().IgnoreAuditedObjectProperties();
        /// CreateMap<MicroServiceFullUser, MicroServiceFullUserCreateUpdateInput>();
        /// CreateMap<MicroServiceFullUserCreateUpdateInput, MicroServiceFullUser>();
        /// CreateMap<MicroServiceFullUserUpdateDto, MicroServiceFullUser>().IgnoreAuditedObjectProperties();
        /// </code>
        /// </example>
        public MicroServiceFullUserProfile()
        {
            CreateMap<MicroServiceFullUser, MicroServiceFullUserGetOutputDto>();
            CreateMap<MicroServiceFullUser, MicroServiceFullUserGetListOutputDto>();
            CreateMap<MicroServiceFullUserGetOutputDto, MicroServiceFullUser>();
            CreateMap<MicroServiceFullUser, MicroServiceFullUserCreateUpdateInput>();
            CreateMap<MicroServiceFullUserCreateUpdateInput, MicroServiceFullUser>();
        }
    }
}
