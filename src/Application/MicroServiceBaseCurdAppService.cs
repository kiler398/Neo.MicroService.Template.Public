﻿using Neo.Application;
using Neo.Domain;
using Neo.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Entities;

namespace NEO.MicroService
{
    public class MicroServiceBaseCurdAppService<TEntity,
                                                TGetOutputDto,
                                                TGetListOutputDto,
                                                TPagedResultDto,
                                                TKey,
                                                TGetListInput,
                                                TCreateInput,
                                                TUpdateInput,
                                                TGetListPageInput,
                                                TRepository,
                                                TManage> :
                             BaseCrudAppService<TEntity,
                                                TGetOutputDto,
                                                TGetListOutputDto,
                                                TPagedResultDto,
                                                TKey,
                                                TGetListInput,
                                                TCreateInput,
                                                TUpdateInput,
                                                TGetListPageInput>
                                                where TKey : struct
                                                where TEntity : class, IEntity<TKey>
                                                where TGetOutputDto : IEntityDto<TKey>
                                                where TGetListOutputDto : IEntityDto<TKey>
                                                where TGetListPageInput : TGetListInput, ISortedResultRequest, IPagedResultRequest
                                                where TPagedResultDto : IPagedResult<TGetListOutputDto>, new()
                                                where TRepository : IBaseAbpRepository<TEntity, TKey>
                                                where TManage : IBaseDomainService<TRepository, TEntity, TKey>
    {

        protected readonly TManage _mainManager;

        public MicroServiceBaseCurdAppService(
            TRepository mainRepository,
            TManage mainManager
        ) : base(mainRepository)
        {
            _mainManager = mainManager;
        }

        public override async Task<TGetOutputDto> CreateAsync(TCreateInput input)
        {
            var entity = await MapToEntityAsync(input);

            await _mainManager.InsertAsync(entity, true);

            return MapToGetOutputDto(entity);
        }

        public override async Task<TGetOutputDto> UpdateAsync(TKey id, TUpdateInput input)
        {
            var updateEntity = await _mainManager.GetAsync(id);

            ObjectMapper.Map<TUpdateInput, TEntity>(input, updateEntity);

            await _mainManager.UpdateAsync(updateEntity);

            return MapToGetOutputDto(updateEntity);
        }

        public virtual async Task DeleteAsync(TEntity entity)
        {
            await _mainManager.DeleteAsync(entity);
        }

        protected override async Task DeleteByIdAsync(TKey id)
        {
            await _mainManager.DeleteByIdAsync(id);
        }

        public override async Task<TGetOutputDto> GetAsync(TKey id)
        {
            return MapToGetOutputDto(await _mainManager.GetAsync(id));
        }

        public async Task InsertManyAsync(List<TCreateInput> createInputs)
        {
            var entities = ObjectMapper.Map<List<TCreateInput>, List<TEntity>>(createInputs);
            await this._mainManager.InsertManyAsync(entities);
        }

        public override async Task<TPagedResultDto> QueryByPage(TGetListPageInput getListPageInput)
        {
            var queryCount = await this.CreateFilteredQueryAsync(getListPageInput);
            var query = await this.CreateFilteredQueryAsync(getListPageInput);

            query = BuildQuery(getListPageInput, query);

            query = query.Skip(getListPageInput.SkipCount);
            query = query.Take(getListPageInput.MaxResultCount);

            var dataCount = queryCount.LongCount();

            var dataList = await MapToGetListOutputDtosAsync(query.ToList());

            TPagedResultDto pagedResultDto = new TPagedResultDto();
            pagedResultDto.Items = dataList;
            pagedResultDto.TotalCount = dataCount;

            return pagedResultDto;
        }


    }
}