﻿# 迁移文档

## 配置文件修改

本地测试建议直接还原生产环境的库到本地，然后做迁移测试

连接到生成服务器的数据库连接字符串，这块建议CI发布的时候动态修改成服务器的这个库专有的迁移用户名和密码

迁移用户名需要拥有添加修改数据库表结构的权限（仅限于这个库）

## 项目运行

直接跑命令行程序迁移数据