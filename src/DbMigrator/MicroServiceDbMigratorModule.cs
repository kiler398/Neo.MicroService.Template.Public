﻿using NEO.MicroService.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace NEO.MicroService.DbMigrator;

[DependsOn(
    typeof(AbpAutofacModule),
    typeof(MicroServiceEntityFrameworkCoreModule),
    typeof(MicroServiceApplicationContractsModule)
    )]
public class MicroServiceDbMigratorModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
    }
}
