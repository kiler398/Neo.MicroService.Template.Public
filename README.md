# Neo.MicroService 微服务项目

Neo.MicroService CLI模板项目

## 快速入门

为了能够快速使用该框架, 下面是一个简要的使用说明文档。

帮助你快速使用框架开发一个项目。

## CLI工具快速初始化项目

- [ ] [安装CLI工具](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [CLI工具使用教程](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/neoteams/newway.abp.demoapp.template.git
git branch -M main
git push -uf origin main
```

## 本地开发环境初始化

- [ ] [初始化本地研发环境](https://gitlab.com/neoteams/newway.abp.demoapp.template/-/settings/integrations)

## 团队指南

- [ ] [ABP DDD最佳实践](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Abp 模块化开发实践](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)

## 框架使用功能指南

框架主要功能介绍

- [ ] [分布式缓存](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [分布式EventBus](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Api返回结果自动包装](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [审计日志使用指南](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [自动化流水线辅助脚本](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [多语言支持](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [异常处理](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [对象映射](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)

## 项目研发流程与规范

为了保证团队代码一致性以及可读性，需要遵守一下规范.

- [ ] [EFCore Code First数据库结构变更规范](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Git代码提交规范](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Git分支管理规范](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [代码质量检查规范](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [代码质量检查规范](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
- [ ] [代码格式化规范](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
- [ ] [API开发规范](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
- [ ] [远程调用规范](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

## 测试项目

基于Xunit的自动化测试框架.

- [ ] [仓库层测试](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [领域服务测试](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [HttpApi测试](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [远程接口调用测试](https://docs.gitlab.com/ee/user/clusters/agent/)

## 生成发布项目

Docker一键发布脚本的使用教程.

项目根目录包含Build.ps1脚本支持CI CD常用操作：

### 发布程序到本地Build目录    

BuildVersionName 为发布版本号，会写入数据迁移和API Host项目

```
 .\Build.ps1 -target "BuildAll" -BuildVersionName "20211108.1"
 ```