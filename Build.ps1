﻿[CmdletBinding()]
param (
	[Alias('target')]
    [string]$BuildTarget,

	[ValidateSet('LocalDocker','DockerRemoteRespository')]
	[Alias('dm')]
    [string]$DeployMethod = "LocalDocker",

	[ValidateSet('PlainText','StoreInFile')]
	[string]$DockerLoginType = "PlainText",

	[Alias('slnPath')]
    [string]$SolutionName = "NEO.MicroService.sln",

    [string]$HostProjectPath = "src/HttpApi.Host",

    [string]$DbMigratorPath = "src/DbMigrator",

    [string]$HostProjectName = "NEO.MicroService.HttpApi.Host.csproj",

    [string]$DbMigratorProjectName = "NEO.MicroService.DbMigrator.csproj",
 
	[string]$HostDockerImageName = "microserviceapi",

	[string]$DbMigratorDockerImageName = "microservicedbmigrator",

	[string]$EnvironmentName = "Production",

	[string]$DeployName = "microservice_dev",

	[bool]$PublishAppToDocker = 0,

	[bool]$DeployAppFromDocker = 0,

	[bool]$IsCheckCodeBuildOnly = 0,

	[Alias('nugetPath')]
    [string]$NuGetConfigFilePath = "NuGet.Config",

	[string]$ProGetServerUrl = "110.42.174.231:8088",

	[string]$ProGetUser = "admin",

	[string]$ProGetPassword = "asdf@123",
	
	[string]$ConsulUrl = "http://localhost:8500",

	[string]$DbConnectionString = "Server=localhost;Port=3306;Database=microservice;user id=lims;Password=asdf@123;allowPublicKeyRetrieval=true;",

	[string]$ProGetServerResponsitoryName = "neo",

	[string]$DockerLocalLibraryName = "library",

	[string]$PackProjectPath = "library",

	[string]$PackProjectDllVersion = "",

	[string]$DeployPackagePath = "build",

	[string]$DeployWebPath = "C:\\WebSites\\MicroService",

	[string]$DockerPasswordFilePath = "/etc/docker_passwd",

	[string]$DeployWebsiteName = "NEO.MicroService",

	[string]$DeployWebsiteApplicationPoolName = "NEO.MicroService",

	[string]$WebHostPort = "9900",

	[bool]$CreateNewIISWebSite = 0,

	[string]$DeployScriptExecutePath = ".\build\",

	[Alias('bv')]
    [string]$BuildVersionName,

	[bool]$EnableSonarCube = 0,

	[string]$SonarProjectName = "DotNetInfrastructure_NEO.MicroService.AbpVNext5",

	[string]$SonarProjectKey = "65bf2c21da6c883a965853dbaee9a74fd763cc2e",

	[string]$SonarHostUrl = "http://192.168.5.64:9000",
 
	[ValidateSet('','Debug','Release')]
	[Alias('c')]
    [string]$Configuration = 'Release'

)

 

# 获取脚本文件所处文件夹的路径
$ScriptFilePath = Split-Path -Parent $MyInvocation.MyCommand.Definition

# 设置当前路径为脚本所处文件夹的路径
Set-Location $ScriptFilePath

# 执行文本命令
function ExecuteShellText($shellText,$shellMsg)
{
	write-host ("执行脚本:$shellText")
	Invoke-Expression "& $shellText"
}

# 构建编译目录
function BuildDir()
{
	New-Item -ItemType "Directory" -Force -Path build/Host
	New-Item -ItemType "Directory" -Force -Path build/DbMigrator
	New-Item -ItemType "Directory" -Force -Path build/DockerImage
	New-Item -ItemType "Directory" -Force -Path $HostProjectPath/build/DockerImage
	New-Item -ItemType "Directory" -Force -Path $DbMigratorPath/build/DockerImage
}

# 向文件写入文本
function WriteTextToFile($filePath,$text)
{
	Write-Output $text >> $filePath
	# Write-Output $filePath > $text
}

# 编译整体解决方案
function BuildSLN()
{
	if($EnableSonarCube)
	{
		ExecuteShellText "dotnet sonarscanner begin /k:""$SonarProjectName"" /d:sonar.host.url=""$SonarHostUrl"" /d:sonar.login=""$SonarProjectKey""" "Sonarcube开始分析代码."
	}
	ExecuteShellText "dotnet build $SolutionName  --configuration $Configuration" "开始编译整个解决方案."
	if($EnableSonarCube)
	{
		ExecuteShellText "dotnet sonarscanner end /d:sonar.login=""$SonarProjectKey""" "Sonarcube结束分析代码."
	}
}

# 还原整体解决方案
function RestoreSLN()
{
	ExecuteShellText "dotnet restore $SolutionName --configfile $NuGetConfigFilePath --verbosity Detailed" "开始还原解决方案."
}

# 发布HOST API项目
function PublishHost()
{
	write-host ("开始发布HostAPI.")

	ExecuteShellText "echo ""$BuildVersionName"" > $HostProjectPath/Version.txt" "更新项目Version.txt"
	
	ExecuteShellText "dotnet publish $HostProjectPath/$HostProjectName --output $HostProjectPath/build/Host /p:Configuration=$Configuration /p:EnvironmentName=$EnvironmentName" "发布HostAPI"
}

# 发布DB Migrator项目
function PublishDbMigrator {
	write-host ("开始发布数据迁移程序.")

	ExecuteShellText "echo  ""$BuildVersionName"" > $DbMigratorPath/Version.txt" "更新项目Version.txt"

	ExecuteShellText "dotnet publish $DbMigratorPath/$DbMigratorProjectName --output $DbMigratorPath/build/DbMigrator /p:Configuration=$Configuration /p:EnvironmentName=$EnvironmentName" "发布DbMigrator程序"
}

function BuildDockerFullImageName($imageName,$buildImageVersionName)
{
	return "$imageName`:$buildImageVersionName"
}

function BuildDockerImageFileName($imageName,$buildImageVersionName) {
	return "$imageName`_$buildImageVersionName`.tar"
}

function BuildDockerImageDeployFileName($imageName,$buildImageVersionName) {
	return "$imageName`_$buildImageVersionName`.sh"
}

# 构建项目Docker镜像
function BuildProjectDockerImage {
	write-host ("开始编译项目docker镜像.")

	BuildDockerImage "$HostProjectPath/Dockerfile" $HostDockerImageName $BuildVersionName "生成HOST镜像"
 
	BuildDockerImage "$DbMigratorPath/Dockerfile" $DbMigratorDockerImageName $BuildVersionName "生成DbMigrator镜像"
}

# 导出项目Docker镜像
function ExportProjectDockerImage {
	write-host ("开始导出项目docker镜像.")

	$fullhostdockerImageFileName = BuildDockerImageFileName $HostDockerImageName $BuildVersionName

	ExportDockerImage "$HostProjectPath/build/DockerImage/$fullhostdockerImageFileName" $HostDockerImageName $BuildVersionName "导出HOST镜像文件"

	$fullhostdockerImageDeployFileName = BuildDockerImageDeployFileName $HostDockerImageName $BuildVersionName

	GenerateAspNetDockerImageDeployScript "$HostProjectPath/build/DockerImage/$fullhostdockerImageDeployFileName" $HostDockerImageName $BuildVersionName "生成HOST镜像部署脚本" $WebHostPort

	$fulldbMigratordockerImageFileName = BuildDockerImageFileName $DbMigratorDockerImageName $BuildVersionName

	ExportDockerImage "$DbMigratorPath/build/DockerImage/$fulldbMigratordockerImageFileName" $DbMigratorDockerImageName $BuildVersionName "导出DbMigrator镜像文件"

	$fulldbMigratordockerImageDeployFileName = BuildDockerImageDeployFileName $DbMigratorDockerImageName $BuildVersionName

	GenerateHostServiceDockerImageDeployScript "$DbMigratorPath/build/DockerImage/$fulldbMigratordockerImageDeployFileName" $DbMigratorDockerImageName $BuildVersionName "生成DbMigrator镜像部署脚本"
}

# 推送项目Docker镜像
function PushProjectDockerImage {
	write-host ("开始推送项目docker镜像.")

	#开始登录Docker
	LoginDocker

	PushDockerImageToProget $HostDockerImageName $BuildVersionName "开始推送HOST镜像文件"

	PushDockerImageToProget $DbMigratorDockerImageName $BuildVersionName "开始推送DbMigrator镜像文件"
}

# 编译docker镜像到本地环境
function BuildDockerImage($dockFilePath,$dockImageName,$dockVersionName,$dockCommentName) {	
	$fullImageName = BuildDockerFullImageName $dockImageName $dockVersionName
	ExecuteShellText "docker build -f $dockFilePath -t=""$fullImageName"" --label ""$dockImageName"" --label ""$dockVersionName"" ." $dockCommentName
}

# 导出Docker镜像
function ExportDockerImage($exportDockerImageFilePath,$dockImageName,$dockVersionName,$dockCommentName) {	
	$fullImageName = BuildDockerFullImageName $dockImageName $dockVersionName
	ExecuteShellText "docker save -o $exportDockerImageFilePath ""$fullImageName""" $dockCommentName
	# Write-Host "删除本地编译镜像"
	# Invoke-Expression "& docker rmi $fullImageName"
}

# 生成自动部署sh脚本
function GenerateAspNetDockerImageDeployScript($dockerImageDeployScriptFilePath,$dockImageName,$dockVersionName,$dockCommentName,$aspNetHostPort){
	$fullImageName = BuildDockerFullImageName $dockImageName $dockVersionName
	$fullImageFileName = BuildDockerImageFileName $dockImageName $dockVersionName
	$portMapper = "$aspNetHostPort`:80"
	$dockerImageDeployScript = "#！bin/sh`
# -------------------------------------------- Deploy Script Start -------------------------------------------- 
docker load -i $fullImageFileName `
docker stop $dockImageName `
docker rm $dockImageName `
docker run -p $portMapper -e App:DeployName=""$DeployName"" -e ServiceDiscovery:ConsulUrl=""$ConsulUrl"" -e ConnectionStrings:Default=""$DbConnectionString"" --name $dockImageName -d $fullImageName `
# -------------------------------------------- Deploy Script End -------------------------------------------- "
	echo $dockerImageDeployScript > $dockerImageDeployScriptFilePath
}

function GenerateHostServiceDockerImageDeployScript($dockerImageDeployScriptFilePath,$dockImageName,$dockVersionName,$dockCommentName){
	$fullImageName = BuildDockerFullImageName $dockImageName $dockVersionName
	$fullImageFileName = BuildDockerImageFileName $dockImageName $dockVersionName
	$dockerImageDeployScript = "#！bin/sh`
# -------------------------------------------- Deploy Script Start -------------------------------------------- 
docker load -i $fullImageFileName `
docker stop $dockImageName `
docker rm $dockImageName `
docker run -e ServiceDiscovery:ConsulUrl=""$ConsulUrl"" -e ConnectionStrings:Default=""$DbConnectionString"" --name $dockImageName -d $fullImageName `
# -------------------------------------------- Deploy Script End -------------------------------------------- "
	echo $dockerImageDeployScript > $dockerImageDeployScriptFilePath
}

function LoginDocker()
{
	Write-Host ("登录Docker仓库.")
	if($DockerLoginType -eq "PlainText")
	{
		Write-Host "开始登录到Proget服务器 : docker login $ProGetServerUrl -u $ProGetUser 明文"
		try {
			Invoke-Expression "& cat $DockerPasswordFilePath | docker login $ProGetServerUrl -u $ProGetUser -p $ProGetPassword " -ErrorAction "SilentlyContinue"   2>&1 >$null	
		}
		catch {
			
		}
	}
	else
	{
		Write-Host ("写入密码.")
		Write-Output $ProGetPassword >> $DockerPasswordFilePath
		Write-Host "开始登录到Proget服务器 : docker login $ProGetServerUrl -u $ProGetUser --password-stdin"
		Invoke-Expression "& cat $DockerPasswordFilePath | docker login $ProGetServerUrl -u $ProGetUser --password-stdin" -ErrorAction "SilentlyContinue"   2>&1 >$null 
	}

}

# 推送Docker镜像到Proget
function PushDockerImageToProget($dockImageName,$dockVersionName,$dockCommentName) {	
	Write-Host $dockCommentName
	$fullImageName = BuildDockerFullImageName $dockImageName $dockVersionName
	# $fullDockerImageName = "$ProGetServerUrl/$ProGetServerResponsitoryName/$DockerLocalLibraryName/$fullImageName"
	Write-Host "开始Tag镜像"
	Invoke-Expression "& docker tag $DockerLocalLibraryName/$fullImageName $ProGetServerUrl/$ProGetServerResponsitoryName/$DockerLocalLibraryName/$fullImageName"
	Write-Host "开始推送镜像"
	Invoke-Expression "& docker push $ProGetServerUrl/$ProGetServerResponsitoryName/$DockerLocalLibraryName/$fullImageName"
	# Write-Host "删除本地编译镜像"
	# Invoke-Expression "& docker rmi $fullImageName"
	# Write-Host "删除本地推送镜像"
	# Invoke-Expression "& docker rmi $fullDockerImageName -f"
}

# 拉取项目Docker镜像进行部署
function DeployProjectDockerFromPullImage {
	write-host ("开始拉取仓库Docker镜像进行部署.")

	#开始登录Docker
	LoginDocker

	PullAndDeployAspNetDockerImageFromProget $HostDockerImageName $BuildVersionName "开始拉取部署HOST镜像文件" $WebHostPort

	PullAndDeployApplicationDockerImageToProget $DbMigratorDockerImageName $BuildVersionName "开始拉取部署DbMigrator镜像文件"
}

# 拉取部署Docker镜像到AspNet生产环境
function PullAndDeployAspNetDockerImageFromProget($dockImageName,$dockVersionName,$dockCommentName,$aspNetHostPort) {	
	Write-Host $dockCommentName
	$fullImageName = BuildDockerFullImageName $dockImageName $dockVersionName
	$fullDockerImageName = "$ProGetServerUrl/$ProGetServerResponsitoryName/$DockerLocalLibraryName/$fullImageName"

	ExecuteShellText "docker pull $fullDockerImageName" "拉取镜像"
	$portMapper = "$aspNetHostPort`:80"
	# 忽略停止容器错误
	Invoke-Expression "& docker stop $dockImageName" -ErrorAction "SilentlyContinue"   2>&1 >$null 
	Invoke-Expression "& docker rm $dockImageName" -ErrorAction "SilentlyContinue"   2>&1 >$null 
 
	ExecuteShellText "docker run -p $portMapper -e App:DeployName=""$DeployName"" -e ServiceDiscovery:ConsulUrl=""$ConsulUrl"" -e ConnectionStrings:Default=""$DbConnectionString"" --name $dockImageName -d $fullDockerImageName " "运行容器"	
}

# 拉取部署Docker镜像到Application生产环境
function PullAndDeployApplicationDockerImageToProget($dockImageName,$dockVersionName,$dockCommentName) {	
	Write-Host $dockCommentName
	$fullImageName = BuildDockerFullImageName $dockImageName $dockVersionName
	$fullDockerImageName = "$ProGetServerUrl/$ProGetServerResponsitoryName/$DockerLocalLibraryName/$fullImageName"
	ExecuteShellText "docker pull $fullDockerImageName" "拉取镜像"
	# 忽略停止容器错误
	Invoke-Expression "& docker stop $dockImageName" -ErrorAction "SilentlyContinue"   2>&1 >$null 
	Invoke-Expression "& docker rm $dockImageName" -ErrorAction "SilentlyContinue"   2>&1 >$null 

	ExecuteShellText "docker run -e ServiceDiscovery:ConsulUrl=""$ConsulUrl"" -e ConnectionStrings:Default=""$DbConnectionString"" --name $dockImageName -d $fullDockerImageName " "运行容器"
}

# 复制所有的发布文件到发布目录
function CopyToBuild() {

	# write-host ("清理镜像发布目录")
	# Remove-Item ./build/DockerImage/ * -recurse

	write-host ("复制所有的发布文件到发布目录.")
	copy-item Build.ps1 ./build -Force
	copy-item $HostProjectPath/build ./ -Recurse -Force
	copy-item $DbMigratorPath/build ./ -Recurse -Force
	
	if ($PublishAppToDocker)
	{
		Remove-Item ./build/DbMigrator/ -recurse
		Remove-Item ./build/Host/ -recurse
		if ($DeployMethod -eq "DockerRemoteRespository")
		{
			Remove-Item ./build/DockerImage/ -recurse
		}
	}
	else
	{
		Remove-Item ./build/DockerImage/ -recurse
	}
	
}

# 发布文件到Docker镜像
function PublishToDocker() {

	write-host ("当前Docker部署模式为：$DeployMethod.")

	If($DeployMethod -eq "LocalDocker" )
	{
		BuildProjectDockerImage
		ExportProjectDockerImage
	}
	Elseif($DeployMethod -eq "DockerRemoteRespository")
	{
		BuildProjectDockerImage
		PushProjectDockerImage
	}
}

function DeployFromDockerToServer(){

	If($DeployMethod -eq "LocalDocker" )
	{
		DeployProjectDockerImage
	}
	Elseif($DeployMethod -eq "DockerRemoteRespository")
	{
		DeployProjectDockerFromPullImage
	}
} 


# 整体打包发布部署文件
function BuildAll {
	if(!$IsCheckCodeBuildOnly)
	{
		BuildDir
	}
	
	RestoreSLN
	BuildSLN
	
	if(!$IsCheckCodeBuildOnly)
	{
		PublishHost
		PublishDbMigrator
		if ($PublishAppToDocker)
		{
			write-host ("开始docker镜像生成。")
			PublishToDocker
		}
		CopyToBuild
	}
}

function FormatSolution() {
	ExecuteShellText "dotnet tool install -g dotnet-format" "安装格式化工具." -ErrorAction "SilentlyContinue"   2>&1 >$null 
	ExecuteShellText "dotnet tool update -g dotnet-format" "升级格式化工具." -ErrorAction "SilentlyContinue"   2>&1 >$null 
	ExecuteShellText "dotnet format $SolutionName" "格式化解决方案."
}

function DeployAll() {
	if ($DeployAppFromDocker)
	{
		DeployFromDockerToServer
	}
	else {
		if($CreateWebsite)
		{
			CreateAppWebsite
		}
		ExcuteDbMigrator
		StopWebsite
		CopyFileToIIS
		RestartWebsite
	}
}
function CreateAppWebsite {
	CreateAppPool $DeployWebsiteAppPoolName
	CreateWebSite $DeployWebsiteName $WebHostPort $DeployWebsiteAppPoolName $DeployAppPath
	# ExecuteShellText "C:\Windows\System32\inetsrv\appcmd.exe add apppool /name:""$DeployWebsiteAppPoolName"" -managedRuntimeVersion """" "
	# $bindPort = "/bindings:http/*:" + $WebHostPort + ":"
	# ExecuteShellText "C:\Windows\System32\inetsrv\appcmd.exe add site /name:""$DeployWebsiteName"" $bindPort  /physicalPath:""$DeployAppPath"" /applicationDefaults.applicationPool:""$DeployWebsiteAppPoolName"" "
}
# # 需要管理员运行
# appcmd.exe set app "default site" /applicationpool:"X"
# C:\Windows\System32\inetsrv\appcmd list apppool /config /xml > c:/apppools.xml
# C:\Windows\System32\inetsrv\appcmd list site /config /xml > c:/sites.xml
# 8 导入所有进程池
# %windir%/system32/inetsrv/appcmd add apppool /in < c:/apppools.xml
# 9 导入所有站点
# %windir%/system32/inetsrv/appcmd add site /in < c:/sites.xml
# C:\Windows\System32\inetsrv\appcmd add site /name:"MyTestSite1" /bindings:http/*:8990: /physicalPath:"E:\TestSite\Site1"
function CreateAppPool {
    param (
        $name,
        $CLRVersion = ""
    )
    $currentPath = get-location
	Import-Module WebAdministration
    set-location iis:\AppPools
    $existsAppPool = test-path $name
    if($existsAppPool -eq $true)
    {
        write-host "应用程序池:$name 已经存在"  -ForegroundColor red
        set-location $currentPath
        exit
    }
    $appPool = new-item $name
    #设置标识：LocalService=1;LocalSystem=2;NewworkService=3;ApplicationPoolIdentity=4
    $appPool.ProcessModel.IdentityType=4
    #设置.NET Framework 版本
    #$appPool.managedRuntimeVersion="v4.0"
    $appPool.managedRuntimeVersion = $CLRVersion
    #设置托管管道模式：集成=0；经典=1
    $appPool.ManagedPipelineMode=0
    $appPool.startMode="AlwaysRunning"
    #设置启用32位应用程序 false=0;true=1
    $appPool.enable32BitAppOnWin64=0
    $appPool | set-item
    set-location $currentPath
}
function CreateWebSite {
    param (
        $name,
        $port,
        $poolName,
        $path
    )
    $currentPath = get-location
	Import-Module WebAdministration
    set-location iis:\sites
    if((test-path $name) -eq $true)
    {
        write-host "IIS站点名： $name 已经存在"  -ForegroundColor red
        set-location $currentPath
        exit
    }
   
    #新建站点
    new-website $name -physicalpath $path -port $port
    #绑定域名
    # new-webbinding -name $name -host $hostname -port 80 -protocol http
    #获取本机IP
    # $ojbItem = Get-WmiObject -Class Win32_NetworkAdapterConfiguration -Filter IPEnabled=TRUE -ComputerName .
    # $ipaddress = $ojbItem.IPAddress[0]
    #绑定IP地址和端口
    # new-webbinding -name $name -protocol http
    #设置应用程序池
    set-itemproperty $name -name applicationpool -value $poolName
    #启用Forms身份验证    
    $config = get-webconfiguration system.web/authentication $name
    $config.mode="Forms"
    $config|set-webconfiguration system.web/authentication
    #启用匿名身份验证
    Set-WebConfigurationProperty -Filter system.webServer/security/authentication/anonymousAuthentication -PSPath MACHINE/WEBROOT/APPHOST -Location $name -Name Enabled -Value $true
    set-location $currentPath
}

function CopyFileToIIS {
	write-host ("复制文件到IIS目录.")
	copy-item .\Host\* $DeployAppPath -Recurse -Force
}

function ExcuteDbMigrator() {
	write-host ("执行数据迁移脚本.")
	$ScriptPath = $DeployScriptExecutePath
	$ScriptPath = Join-Path $ScriptPath "DbMigrator"
	ExecuteShellText "cd $ScriptPath" ""
	ExecuteShellText "dotnet TONO.MicroService.DbMigrator.dll" ""
	ExecuteShellText "cd.." ""
}




function StopWebsite() {
	write-host ("停止网站.")
	# IIS应用程序离线开关文件
	$appOfflineFilePath = Join-Path $DeployAppPath -ChildPath "app_offline.htm"
	
	# 应用程序离线
	New-Item -Path $appOfflineFilePath -ItemType "file" -Force
	
	# 等待应用程序离线
	Write-Host ("等待应用程序离线.")
	Start-Sleep -Seconds 3
 
}
function RestartWebsite() {
	write-host ("重启网站.")
	# IIS应用程序离线开关文件
	$appOfflineFilePath = Join-Path $DeployAppPath -ChildPath "app_offline.htm"
	
	# 应用程序上线
	Remove-Item $appOfflineFilePath -Force -Recurse
}


# 压缩文件
function CompressToZip($compressFolder,$compressFile) {
	Compress-Archive -LiteralPath $compressFolder -DestinationPath $compressFile
}

# 解压文件
function ExtractZipFile($zipFile,$extractFolder) {
	Expand-Archive -LiteralPath $zipFile -DestinationPath $extractFolder
}
 
# 本地部署Docker镜像
function DeployProjectDockerImage {
	write-host ("开始本地部署Docker镜像.")

	DeployAspNetDockerImage $HostDockerImageName $BuildVersionName "开始本地部署HOST镜像文件" $WebHostPort

	DeployApplicationDockerImage $DbMigratorDockerImageName $BuildVersionName "开始本地部署DbMigrator镜像文件"
}


function DeployAspNetDockerImage($dockImageName,$dockVersionName,$dockCommentName,$aspNetHostPort) {
	write-host ($dockCommentName)

	$fullImageName = BuildDockerFullImageName $dockImageName $dockVersionName
	$fullImageFileName = BuildDockerImageFileName $dockImageName $dockVersionName
	$portMapper = "$aspNetHostPort`:80"
	Invoke-Expression "& docker load -i $fullImageFileName "
	# 忽略停止容器错误
	try
	{
		Invoke-Expression "& docker stop $dockImageName "
		Invoke-Expression "& docker rm $dockImageName "
	}
	catch
	{
	}
	Invoke-Expression "& docker run -p $portMapper -e App:DeployName=""$DeployName"" -e ServiceDiscovery:ConsulUrl=""$ConsulUrl"" -e ConnectionStrings:Default=""$DbConnectionString"" --name $dockImageName -d $fullImageName "
}

function DeployApplicationDockerImage($dockImageName,$dockVersionName,$dockCommentName) {
	write-host ($dockCommentName)

	$fullImageName = BuildDockerFullImageName $dockImageName $dockVersionName
	$fullImageFileName = BuildDockerImageFileName $dockImageName $dockVersionName
	Invoke-Expression "& docker load -i $fullImageFileName "
	# 忽略停止容器错误
	try
	{
		Invoke-Expression "& docker stop $dockImageName "
		Invoke-Expression "& docker rm $dockImageName "
	}
	catch
	{
	}
	Invoke-Expression "& docker run -e ServiceDiscovery:ConsulUrl=""$ConsulUrl"" -e ConnectionStrings:Default=""$DbConnectionString"" --name $dockImageName -d $fullImageName "
}

if($BuildTarget -eq '')
{
	write-host ("未发现发现编译目标:$BuildTarget，操作跳过。")
}
else
{
	& $BuildTarget
}


# 发布Linux Docker 仓库版本   .\Build.ps1 -target "BuildAll" -BuildVersionName "20211220.03" -PublishAppToDocker 1 -DeployMethod "DockerRemoteRespository" -EnableSonarCube 1
# 发布Linux Docker 本地镜像   .\Build.ps1 -target "BuildAll" -BuildVersionName "20211220.01" -PublishAppToDocker 1 -DeployMethod "LocalDocker"
# 发布程序到本地Build目录     .\Build.ps1 -target "BuildAll" -BuildVersionName "20211108.1"


# 格式化项目   .\Build.ps1 -target "FormatSolution"
# 生成发布目录   .\Build.ps1 -target "BuildDir"
# 还原解决方案   .\Build.ps1 -target "RestoreSLN"
# 编译解决方案   .\Build.ps1 -target "BuildSLN"
# 发布Host项目   .\Build.ps1 -target "PublishHost" -BuildVersionName "20211108.1"
# 发布DbMigrator数据迁移程序   .\Build.ps1 -target "PublishDbMigrator" -BuildVersionName "20211108.1"
# 发布Docker镜像   .\Build.ps1 -target "BuildProjectDockerImage" -BuildVersionName "20211108.1"
# 导出Docker镜像   .\Build.ps1 -target "ExportProjectDockerImage" -BuildVersionName "20211108.1"
# 推送Docker镜像到Proget   .\Build.ps1 -target "PushProjectDockerImage" -BuildVersionName "20211108.1"
# 把所有发布文件复制到发布目录   .\Build.ps1 -target "CopyToBuild"


 


 



